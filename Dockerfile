FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

ARG gm_key
ENV REACT_APP_GOOGLEMAPS_APIKEY=$gm_key

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["HumansVsZombies/HumansVsZombies.csproj", "HumansVsZombies/"]
RUN dotnet restore "HumansVsZombies/HumansVsZombies.csproj"
COPY . .
WORKDIR "/src/HumansVsZombies"
RUN dotnet build "HumansVsZombies.csproj" -c Release -o /app/build

FROM build AS publish
RUN curl -sL https://deb.nodesource.com/setup_12.x |  bash -
RUN apt-get install -y nodejs
RUN dotnet publish "HumansVsZombies.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "HumansVsZombies.dll"]
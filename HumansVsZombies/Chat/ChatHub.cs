﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;


namespace HumansVsZombies.Chat
{
    public class ChatHub : Hub
    {

        public async Task SendToAll(string playerName, bool isHuman, bool isGameAdmin, int gameId, string message)
        {

            await Clients.All.SendAsync("ReceiveAllMessage", playerName, isHuman, isGameAdmin, gameId, message);
        }
    }
}

﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;


namespace HumansVsZombies.Chat
{
    public class HumanChatHub : Hub
    {
        public async Task SendToHumans(string playerName, int gameId, string message)
        {

            await Clients.All.SendAsync("ReceiveHumanMessage", playerName, gameId, message);
        }
    }
}

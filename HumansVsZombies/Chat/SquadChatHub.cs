﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;


namespace HumansVsZombies.Chat
{
    public class SquadChatHub : Hub
    {
        public async Task SendToSquad(string playerName, bool isLeader,  int gameId, int squadId, string message)
        {

            await Clients.All.SendAsync("ReceiveSquadMessage", playerName, isLeader, gameId, squadId, message);
        }

    }
}

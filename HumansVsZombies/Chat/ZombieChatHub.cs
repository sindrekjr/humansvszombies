﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;


namespace HumansVsZombies.Chat
{
    public class ZombieChatHub : Hub
    {
        public async Task SendToZombies(string playerName, int gameId, string message)
        {

            await Clients.All.SendAsync("ReceiveZombieMessage", playerName, gameId, message);
        }  
    }
}

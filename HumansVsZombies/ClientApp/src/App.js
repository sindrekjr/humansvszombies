import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import ApiAuthorizationRoutes from './components/api-authorization/ApiAuthorizationRoutes';
import { ApplicationPaths } from './components/api-authorization/ApiAuthorizationConstants';

import './custom.css'
import HomePage from './components/Home/HomePage';
import GameDetail from './components/Game/GameDetail';
import AdminView from './components/Admin/AdminView';
import AdminViewUpdateGame from './components/Admin/AdminViewUpdateGame';
import SquadHome from './components/Squad/SquadHome';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={HomePage} />
        <Route exact path='/game/:id' component={GameDetail} />
        <Route exact path='/game/:gameId/squad/:id' component={SquadHome} />
        <Route exact path='/admin' component={AdminView} />
        <Route exact path='/admin/game' component={AdminViewUpdateGame} />
        <Route path={ApplicationPaths.ApiAuthorizationPrefix} component={ApiAuthorizationRoutes} />
      </Layout>
    );
  }
}

import React, { Component } from "react";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import AdminViewUpdateGame from "./AdminViewUpdateGame";
import AdminViewPlayers from "./AdminViewPlayers";
import authService from "../api-authorization/AuthorizeService";

let token;

class AdminView extends Component {
  _isMounted = false;

  state = {
    game: {},
    games: [],
    loggedInUserId: null
  };

  async componentDidMount() {
    this._isMounted = true;
    this.getLoggedInUserId()

    try {
      token = await authService.getAccessToken()
      
      const response = await fetch("api/game", {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token
        }
      });
      const json = await response.json();
      if(this._isMounted){
        this.setState({
          games: json,
          game: {},
          token
        });
      }
      
    } catch (e) {
      console.error(e);
    }
    
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getLoggedInUserId = async () => {
    try {
      token = await authService.getAccessToken()
      
      fetch("api/game/me", {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token
        }
      }).then(response => {
        if(!response.ok){
          // back to homepage if not logged in
          this.props.history.push('/')
        }
        return response.text()
      }).then(data => {
        if(this._isMounted){
          this.setState({
            loggedInUserId: data
          })
        } 
      })
      
    } catch (e) {
      console.error(e)
    }
  }

  async viewGame(game) {
    this.inputName.value = game.name;
    this.radioState.value = parseInt(game.gameState);
  }

  async createGame(e) {
    e.preventDefault();
    var form = e.target;
    await this.setState({
      game: {
        name: this.inputName.value,
        gameState: parseInt(form.elements.radioState.value)
      }
    });
    
    this.postGame();
  }

  async postGame() {
    const response = await fetch("api/game", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.state.game)
    });
    await response.json();
    await setTimeout(3000);
    //this.props.history.push("/admin/game");
    window.location.reload();
  }

  render() {
    let gamesUserIsAdminIn = this.state.games.filter(game => Object.keys(game.players.filter(player => player.userId === this.state.loggedInUserId)).length > 0)
    let token = this.state.token
    return (
      <div>
        <Tabs defaultActiveKey="home" id="uncontrolled-tab-example">
          <Tab eventKey="home" title="Update games">
            <AdminViewUpdateGame games={gamesUserIsAdminIn} token={token}></AdminViewUpdateGame>
          </Tab>
          <Tab eventKey="profile" title="Players">
            <AdminViewPlayers games={gamesUserIsAdminIn} token={token}></AdminViewPlayers>
          </Tab>
          <Tab eventKey="contact" title="Mission markers" disabled>
            fdsfsd
          </Tab>
        </Tabs>
      </div>
     
    );
  }
}

export default AdminView;

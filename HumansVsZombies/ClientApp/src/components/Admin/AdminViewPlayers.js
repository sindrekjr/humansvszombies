import React, { Component } from "react";

let bitecode;

class AdminViewPlayers extends Component {
  state = {
    game: {},
    games: [],
    player: {},
    players: []
  };

  async viewPlayer(player) {
    this.inputId.value = player.id;
    this.inputGameId.value = player.gameId;
    this.inputPlayerName.value = player.playerName;
    this.inputUserId.value = player.userId;
    this.selectHuman.value = player.isHuman.toString();
    this.selectPatientZero.value = player.isPatientZero.toString();
    this.selectAdmin.value = player.isGameAdmin.toString();
    bitecode = player.biteCode;
  }

  async updatePlayer(e) {
    e.preventDefault();
    await this.setState({
      player: {
        id: parseInt(this.inputId.value),
        gameId: parseInt(this.inputGameId.value),
        playerName: this.inputPlayerName.value,
        userId: this.inputUserId.value,
        isHuman: JSON.parse(this.selectHuman.value),
        isPatientZero: JSON.parse(this.selectPatientZero.value),
        isGameAdmin: JSON.parse(this.selectAdmin.value),
        biteCode: bitecode
      }
    });
    this.putPlayer();
  }

  async putPlayer() {
    var gameid = this.state.player.gameId;
    const url = "api/game/" + gameid + "/player/" + this.state.player.id;
    await fetch(url, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.token
      },
      body: JSON.stringify(this.state.player)
    });
    window.location.reload();
  }

  async deletePlayer() {
    const gameid = this.inputGameId.value;
    const playerid = this.inputId.value;
    await fetch("api/game/" + gameid + "/player/" + playerid, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + this.props.token
      }
    });
    window.location.reload();
  }

  async viewGame(game) {
    let selectedGame = game.id;
    const response = await fetch("api/game/" + selectedGame + "/player", {
      method: "GET",
      headers: {
        Authorization: "Bearer " + this.props.token
      }
    });
    const json = await response.json();
    await this.setState({
      players: json
    });
  }

  render() {
    return (
      <div>
        <div className="games-container">
          <h1>Your games</h1>
          <table className="table table-striped table-hover table-dark">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Game state</th>
              </tr>
            </thead>
            <tbody>
              {this.props.games.map((game, i) => (
                <tr key={i} onClick={() => this.viewGame(game)}>
                  <td>{game.id}</td>
                  <td>{game.name}</td>
                  <td>{game.gameState}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="players-container">
          <h1>All players</h1>
          <table className="table table-striped table-hover table-dark">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Game ID</th>
                <th scope="col">Player Name</th>
                <th scope="col">User ID</th>
                <th scope="col">Human</th>
                <th scope="col">Patient Zero</th>
                <th scope="col">Game Admin</th>
              </tr>
            </thead>
            <tbody>
              {this.state.players.map((player, i) => (
                <tr key={i} onClick={() => this.viewPlayer(player)}>
                  <td>{player.id}</td>
                  <td>{player.gameId}</td>
                  <td>{player.playerName}</td>
                  <td>{player.userId}</td>
                  <td>{player.isHuman.toString()}</td>
                  <td>{player.isPatientZero.toString()}</td>
                  <td>{player.isGameAdmin.toString()}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <form onSubmit={this.updatePlayer.bind(this)}>
          <div className="form-group">
            <input
              className="form-control"
              ref={input => (this.inputId = input)}
              type="number"
              placeholder="ID"
              readOnly
            ></input>
            <input
              className="form-control"
              ref={input => (this.inputUserId = input)}
              type="text"
              placeholder="User ID"
              readOnly
            ></input>
            <input
              className="form-control"
              ref={input => (this.inputGameId = input)}
              type="number"
              placeholder="Game ID"
              readOnly
            ></input>
            <input
              className="form-control"
              ref={input => (this.inputPlayerName = input)}
              type="text"
              placeholder="Player name"
            ></input>
            <div className="form-group">
              <label htmlFor="exampleFormControlSelect1">Human</label>
              <select
                className="form-control"
                id="exampleFormControlSelect1"
                ref={select => (this.selectHuman = select)}
              >
                <option>true</option>
                <option>false</option>
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="exampleFormControlSelect1">Patient Zero</label>
              <select
                className="form-control"
                id="exampleFormControlSelect1"
                ref={select => (this.selectPatientZero = select)}
              >
                <option>true</option>
                <option>false</option>
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="exampleFormControlSelect1">Admin</label>
              <select
                readOnly
                className="form-control"
                id="exampleFormControlSelect1"
                ref={select => (this.selectAdmin = select)}
              >
                <option>true</option>
                <option>false</option>
              </select>
            </div>
          </div>
          <br />
          <button type="submit" className="btn btn-primary">
            Update player
          </button>
          <span>&nbsp;&nbsp;</span>
          <button
            className="btn btn-danger"
            onClick={this.deletePlayer.bind(this)}
          >
            Delete player
          </button>
        </form>
      </div>
    );
  }
}

export default AdminViewPlayers;

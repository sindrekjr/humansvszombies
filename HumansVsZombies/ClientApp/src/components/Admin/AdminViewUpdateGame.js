import React, { Component } from "react";

class AdminViewUpdateGame extends Component {
  state = {
    game: {},
  };

  async viewGame(game) {
    this.inputName.value = game.name;
    this.inputId.value = game.id;
  }

  async updateGame(e) {
    e.preventDefault();
    var form = e.target;
    await this.setState({
      game: {
        id: parseInt(this.inputId.value),
        name: this.inputName.value,
        gameState: parseInt(form.elements.radioState.value)
      }
    });
    this.putGame();
  }

  async putGame() {
    await fetch("api/game/" + this.inputId.value, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.token
      },
      body: JSON.stringify(this.state.game)
    });
    window.location.reload();
  }

  async deleteGame() {
    await fetch("api/game/" + this.inputId.value, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + this.props.token
      }
    });
    window.location.reload();
  }

  render() {
    return (
      <div className="games-container">
        <h1>Your games</h1>
        <table className="table table-striped table-hover table-dark">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Game state</th>
            </tr>
          </thead>
          <tbody>
            {this.props.games.map((game, i) => (
              <tr key={i} onClick={() => this.viewGame(game)}>
                <td>{game.id}</td>
                <td>{game.name}</td>
                <td>{game.gameState}</td>
              </tr>
            ))}
          </tbody>
        </table>
        <br />
        <form onSubmit={this.updateGame.bind(this)}>
          <div className="form-group">
            <input
              className="form-control"
              ref={input => (this.inputId = input)}
              type="number"
              placeholder="Game ID"
            ></input>
            <input
              className="form-control"
              ref={input => (this.inputName = input)}
              type="text"
              placeholder="Game name"
            ></input>
          </div>
          <br />
          <h4>Game state</h4>
          <div className="form-check">
            <input
              className="form-check-input"
              ref={radio => (this.radioState = radio)}
              type="radio"
              name="radioState"
              id="exampleRadios1"
              value="0"
            />
            <label className="form-check-label" htmlFor="exampleRadios1">
              Registration
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              ref={radio => (this.radioState = radio)}
              type="radio"
              name="radioState"
              id="exampleRadios2"
              value="1"
              defaultChecked
            />
            <label className="form-check-label" htmlFor="exampleRadios2">
              In progress
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              ref={radio => (this.radioState = radio)}
              type="radio"
              name="radioState"
              id="exampleRadios3"
              value="2"
            />
            <label className="form-check-label" htmlFor="exampleRadios3">
              Completed
            </label>
          </div>
          <br />
          <button type="submit" className="btn btn-primary">
            Update game
          </button>
          <span>&nbsp;&nbsp;</span>
          <button
            className="btn btn-danger"
            onClick={this.deleteGame.bind(this)}
          >
            Delete game
          </button>
        </form>
      </div>
    );
  }
}

export default AdminViewUpdateGame;

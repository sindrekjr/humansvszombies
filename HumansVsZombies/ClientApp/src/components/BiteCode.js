import React, { Component } from "react";
import authService from "./api-authorization/AuthorizeService";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

/* Bite Code Fragment. When tagged, human players are required to provide
a unique, secret bite code to the zombie. The bite codes should be randomly
generated and appropriate for manual text entry. */

class BiteCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      game: props.game,
      player: props.player,
      players: [],
      kill: {},
      victim: {},
      userLocation: {}
    };
  }

  async componentDidMount() {
      try {
        const response = await fetch(
          "api/game/" + this.state.game.id + "/player"
        );
        const json = await response.json();

        await this.setState({
          players: json
        });
      } catch (e) {
        console.error(e);
      }
      navigator.geolocation.getCurrentPosition(
        position => {
          const { latitude, longitude } = position.coords;
    
          this.setState({
            userLocation: { lat: latitude, lng: longitude }
          });
    }
    );
  }

  async addKill(e) {
    e.preventDefault();
    let story = this.inputStory.value;
    if (story === "") {
      story = "No info";
    }
    let date = new Date()
      .toISOString()
      .replace(",", "")
      .replace(/:.. /, " ");
    await this.setState({
      kill: { 
        killerId: this.props.player.id,
        gameId: this.state.game.id,
        biteCode: this.inputBiteCode.value,
        story: story,
        timeOfDeath: date,
        Lat: this.state.userLocation.lat,
        Lng: this.state.userLocation.lng
      }
    });
    this.killPlayer();
  }

  async killPlayer() {
    const token = await authService.getAccessToken();
    const url = "api/game/" + this.state.game.id + "/kill";
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token
        },
        body: JSON.stringify(this.state.kill)
      });
      if (response.status === 201) {
        alert("Kill confirmed");
      } else if (response.status === 400) {
        alert("Kill failed. Are you sure the bite code is correct?");
      }
      await response.json().then(() => {
        this.props.fetchKills()
        document.getElementById("bite-code-form").reset();
      })
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    if (this.props.player.isHuman === true && this.state.game.gameState !== "Complete") {
      return (
        <Card bg="dark" border="secondary">
          <Card.Header>Your BiteCode</Card.Header>
          <Card.Body as="h5">{this.props.player.biteCode}</Card.Body>
        </Card>
      );
    } else if (this.props.player.isHuman === false && this.state.game.gameState !== "Complete") {
      return (
        <Card bg="dark" border="secondary">
          <Card.Header>Bite Code</Card.Header>
          <Card.Body>
            <Form id="bite-code-form" onSubmit={this.addKill.bind(this)}>
              <Form.Group controlId="formBiteCode">
                <Form.Control
                  type="text"
                  placeholder="Enter Bite Code"
                  required
                  ref={input => (this.inputBiteCode = input)}
                />
                <Form.Text className="text-muted">
                  Enter a human's bite code to confirm a kill.
                </Form.Text>
              </Form.Group>
              <Form.Group>
                <Form.Control
                  type="text"
                  placeholder="Story"
                  ref={input => (this.inputStory = input)}
                />
                <Form.Text className="text-muted">
                  Enter some information about the kill (optional).
                </Form.Text>
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          </Card.Body>
        </Card>
      );
    } else return null;
  }
}

export default BiteCode;

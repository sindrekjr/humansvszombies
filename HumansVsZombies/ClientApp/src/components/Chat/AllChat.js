import React, { Component } from 'react';
import * as signalR from '@aspnet/signalr';
import { ListGroup, InputGroup, Card, CardHeader, Button, Input, InputGroupAddon, ListGroupItem } from 'reactstrap';


export class AllChat extends Component {
    constructor(props) {
        super(props);

        this.state = {
            gameId: this.props.gameId,
            message: '',
            messages: [],
            hubConnection: null,
        };
    }

    componentDidMount = () => {

      // set the connection
        const hubConnection = new signalR.HubConnectionBuilder()
            .withUrl("/api/chat")
            .configureLogging(signalR.LogLevel.Information)
            .build();

        this.setState({
            hubConnection: hubConnection
        });

        //Disable send button until connection is established
        document.getElementById("sendAllButton").disabled = true;

        // Recieve a message from the allchat
        hubConnection.on("ReceiveAllMessage", (playerName, isHuman, isGameAdmin, gameId, message) => {
          if(gameId === this.state.gameId){
            let msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
            let playerMessage = {playerName: playerName,isHuman: isHuman, isGameAdmin: isGameAdmin, message: msg}
            const messages = this.state.messages.concat([playerMessage]);
            this.setState({ messages });
            //scroll to the bottom
            let list = document.getElementById("message-list-all");
            list.scrollTop = list.offsetHeight;
          }
        });

        // Start the connection and enable the sendbutton
        hubConnection.start().then(function () {
            document.getElementById("sendAllButton").disabled = false;
        }).catch(function (err) {
            return console.error(err.toString());
        });
    };

    // Send a message to everyone in this game
    sendMessage = () => {
        this.state.hubConnection
            .invoke('SendToAll', this.props.player.playerName, this.props.player.isHuman, this.props.player.isGameAdmin, this.state.gameId, this.state.message)
            .catch(err => console.error(err));

        this.setState({ message: '' });
    };

    render() {
      const scrollable = {
        maxHeight: '300px',
        minHeight: '300px',
        overflow: 'auto'
      };

        return (
          <div>
          <br/>
            <Card className="bg-dark">
              <CardHeader>Global Chat</CardHeader>
                <ListGroup id="message-list-all" style={scrollable}>
                    {this.state.messages.map((message, index) => (
                        <ListGroupItem className="bg-dark" key={index}> 
                          <span className={(message.isGameAdmin ? "text-success " :  !message.isHuman ? "text-danger" : "text-info")}>{message.playerName}</span> : {message.message} 
                        </ListGroupItem>
                    ))}
                </ListGroup>

                <InputGroup>
                  <Input
                  className="bg-dark"
                  style={{color: "#b3c5c8", border: "solid #6c757d"}}
                  value={this.state.message}
                  onChange={e => this.setState({ message: e.target.value })}
                  onKeyPress={event => {
                    if (event.key === 'Enter') {
                      this.sendMessage();
                    }
                  }}
                  />
                  <InputGroupAddon addonType="append">
                    <Button id="sendAllButton" onClick={this.sendMessage}>Send To All</Button>
                  </InputGroupAddon>
                </InputGroup>
            </Card>
            </div>
        );
    }
}

export default AllChat;
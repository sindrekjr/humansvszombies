import React, { Component } from 'react';
import * as signalR from '@aspnet/signalr';
import { ListGroup, InputGroup, Card, CardHeader, Button, Input, InputGroupAddon, ListGroupItem } from 'reactstrap';


export class SquadChat extends Component {
    constructor(props) {
        super(props);

        this.state = {
            player: this.props.player,
            gameId: this.props.gameId,
            squadId: this.props.squadId,
            message: '',
            messages: [],
            hubConnection: null,
        };
    }

    componentDidMount = () => {

      // set the connection
        const hubConnection = new signalR.HubConnectionBuilder()
            .withUrl("/api/chat/squad")
            .configureLogging(signalR.LogLevel.Information)
            .build();

        this.setState({
            hubConnection: hubConnection
        });

        //Disable send button until connection is established
        document.getElementById("sendSquadButton").disabled = true;

        // Recieve a message from the allchat
        hubConnection.on("ReceiveSquadMessage", (playerName, isLeader, gameId, squadId, message) => {
          if(gameId === this.state.gameId && this.state.squadId === squadId){
            var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
            var playerMessage = {playerName: playerName, isLeader: isLeader, message: msg}
            const messages = this.state.messages.concat([playerMessage]);
            this.setState({ messages });
            //scroll to the bottom
            let list = document.getElementById("message-list-squad");
            list.scrollTop = list.offsetHeight;
          }
        });

        // Start the connection and enable the sendbutton
        hubConnection.start().then(function () {
            document.getElementById("sendSquadButton").disabled = false;
        }).catch(function (err) {
            return console.error(err.toString());
        });
    };

    // Send a message to squad
    sendMessage = () => {
      const player = this.state.player
      let isLeader = false;
      player.squadMembers.forEach(sm => {
        if(sm.id === this.state.gameId){
          if(sm.rank === "Leader"){
            isLeader = true;
          }
        }
      });
        this.state.hubConnection
            .invoke('SendToSquad', player.playerName, isLeader, this.state.gameId,this.state.squadId, this.state.message)
            .catch(err => console.error(err));

        this.setState({ message: '' });
    };

    render() {
      const scrollable = {
        maxHeight: '300px',
        minHeight: '300px',
        overflow: 'auto'
      };
        return (
          <div>
          <br/>
            <Card className="bg-dark">
                <CardHeader>Chat</CardHeader>
                <ListGroup id="message-list-squad" style={scrollable}>
                    {this.state.messages.map((message, index) => (
                        <ListGroupItem className="bg-dark" key={index}> 
                          <span className={(message.isLeader ? "text-success" : "")}>{message.playerName}</span> : {message.message} 
                        </ListGroupItem>
                    ))}
                </ListGroup>
                     
                <InputGroup>
                  <Input
                  className="bg-dark"
                  style={{color: "#b3c5c8", border: "solid #6c757d"}}
                  value={this.state.message}
                  onChange={e => this.setState({ message: e.target.value })}
                  onKeyPress={event => {
                    if (event.key === 'Enter') {
                      this.sendMessage();
                    }
                  }}
                  />
                  <InputGroupAddon addonType="append">
                    <Button id="sendSquadButton" onClick={this.sendMessage}>Send To Squad</Button>
                  </InputGroupAddon>
                </InputGroup>
            </Card>
            </div>
        );
    }
}

export default SquadChat;
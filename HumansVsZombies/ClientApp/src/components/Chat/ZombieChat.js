import React, { Component } from 'react';
import * as signalR from '@aspnet/signalr';
import { ListGroup, InputGroup, Card, CardHeader, Button, Input, InputGroupAddon, ListGroupItem } from 'reactstrap';


export class ZombieChat extends Component {
    constructor(props) {
        super(props);

        this.state = {
            player: this.props.player,
            gameId: this.props.gameId,
            message: '',
            messages: [],
            hubConnection: null,
        };
    }

    componentDidMount = () => {

      // set the connection
        const hubConnection = new signalR.HubConnectionBuilder()
            .withUrl("/api/chat/zombie")
            .configureLogging(signalR.LogLevel.Information)
            .build();

        this.setState({
            hubConnection: hubConnection
        });

        //Disable send button until connection is established
        document.getElementById("sendZombieButton").disabled = true;

        // Recieve a message from the allchat
        hubConnection.on("ReceiveZombieMessage", (playerName, gameId, message) => {
          if(gameId === this.state.gameId){
            var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
            var playerMessage = {playerName: playerName, message: msg}
            const messages = this.state.messages.concat([playerMessage]);
            this.setState({ messages });
            //scroll to the bottom
            let list = document.getElementById("message-list-zombie");
            list.scrollTop = list.offsetHeight;
          }
        });

        // Start the connection and enable the sendbutton
        hubConnection.start().then(function () {
            document.getElementById("sendZombieButton").disabled = false;
        }).catch(function (err) {
            return console.error(err.toString());
        });
    };

    // Send a message to zombies in this game
    sendMessage = () => {
      const player = this.props.player;
        this.state.hubConnection
            .invoke('SendToZombies', player.playerName, this.state.gameId, this.state.message)
            .catch(err => console.error(err));

        this.setState({ message: '' });
    };

    render() {
      const scrollable = {
        maxHeight: '300px',
        minHeight: '300px',
        overflow: 'auto'
      };
        return (
          <div>
          <br/>
            <Card className="bg-dark">
                <CardHeader>Zombie Chat</CardHeader>
                <ListGroup id="message-list-zombie" style={scrollable}>
                    {this.state.messages.map((message, index) => (
                        <ListGroupItem className="bg-dark" key={index}> {message.playerName} : {message.message} </ListGroupItem>
                    ))}
                </ListGroup>
                     
                <InputGroup>
                  <Input
                  className="bg-dark border-danger"
                  style={{color: "#b3c5c8"}} 
                  value={this.state.message}
                  onChange={e => this.setState({ message: e.target.value })}
                  onKeyPress={event => {
                    if (event.key === 'Enter') {
                      this.sendMessage();
                    }
                  }}
                  />
                  <InputGroupAddon addonType="append">
                    <Button id="sendZombieButton" onClick={this.sendMessage} color='danger'>Send To Zombies</Button>
                  </InputGroupAddon>
                </InputGroup>
            </Card>
            </div>
        );
    }
}

export default ZombieChat;
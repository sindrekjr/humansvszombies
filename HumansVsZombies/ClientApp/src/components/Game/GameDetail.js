import React, { Component } from "react";
import GameDetailTitle from "./GameDetailTitle";
import GameDetailRegistration from "./GameDetailRegistration";
import GameStart from "./GameStart";
import BiteCode from "../BiteCode";
import SquadRegistration from "./SquadRegistration";
import SquadDetails from "./SquadDetails";
import AllChat from "../Chat/AllChat";
import HumanChat from "../Chat/HumanChat";
import ZombieChat from "../Chat/ZombieChat";
import "./GameDetail.css";
import authService from "../api-authorization/AuthorizeService";
import Map from '../Map/Map';

/* The game detail page that shows details about a game. The different detail parts are composed of different fragments in separate react components  */

/*   "The visibility of each of these fragments by game and player state is
described in Table 3.1. (page 9/19)" */

const mapStyles = {
  width: "600px",
  height: "400px"
};

class GameDetail extends Component {
  state = {
    games: [],
    game: {},
    kills: [],
    token: null,
    player: {},
    loading: false,
    squads: {}
  };

  componentDidMount = async () => {
    
    // Set token
    const token = await authService.getAccessToken();
    await this.setState({ token: token });

    this.load();
  };

  async load() {
    const url = new URL(window.location.href);

    //set Kills
    fetch("/api" + url.pathname + "/kill").then(response =>
      response.json()
    ).then(data => 
      this.setState({ kills: data })
    );

    // Get logged in player
    await this.getPlayer();

    //get current game
    await this.getGame();

    // get squads
    await this.getSquads();
  }

  getSquads = async () => {
    try {
      const response = await fetch("api/game/" + this.state.game.id + "/squad");
      const json = await response.json();

      await this.setState({
        squads: json
      });
    } catch (e) {
      console.error(e);
    }
  }

  // Append a squad to the current list of squads
  addSquad = async (newSquad) => {
    const newKey = Object.keys(this.state.squads).length
    let newSquads = this.state.squads
    newSquads[newKey] = newSquad
    this.setState({
      squads: newSquads
    })
    this.load();
  }

  // Append a squadmember to the current list of squadmembers
  addSquadMember = async (newSquadMember) => {
    await setTimeout(100)
    this.getPlayer()
    this.getSquads()
    this.load();
  }

  // Remove squadMember from the current list(locally)
  removeSquadMember = async (deletedSquadMember) => {
    await setTimeout(100)
    this.getPlayer()
    this.getSquads()
    this.load();
  }
  addRegisteredPlayer = async (registeredPlayer) => {
    this.setState({
      player: registeredPlayer
    })
    this.load();
  }


  // get the current logged in player
  getPlayer = async () => {
    try {
      const url = new URL(window.location.href);
      const response = await fetch("api" + url.pathname + "/player/me", {
        method: "GET",
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      });
      const json = await response.json();

      await this.setState({
        player: json
      });
    } catch (e) {
      // Do nothing for now
    }
  };

  fetchKills = async () => {
    const url = new URL(window.location.href);

    const data = await fetch("/api" + url.pathname + "/kill").then(response =>
      response.json()
    );
    await this.setState({ kills: data });
    this.forceUpdate()
  }

  getGame = async () => {
    try {
      const url = new URL(window.location.href);

      const response = await fetch("api" + url.pathname, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      });
      const json = await response.json();

      await this.setState({
        game: json,
        loading: true
      });
    } catch (e) {
      console.error(e);
    }
  };

  render() {
    const game = this.state.game;
    const player = this.state.player;
    if (this.state.loading === true) {
      var humans = game.players.filter(function(s) {
        return s.isHuman && s.death == null;
      }).length;
      var zombies = game.players.filter(function(s) {
        return !s.isHuman;
      }).length;
      var squads = game.squads.length;
    }

    if (this.state.loading === true) {
      const startTime = new Date(game.startTime);
      const endTime = new Date(game.endTime);

      return (
        <div>
          <GameDetailTitle game={game}></GameDetailTitle>
          <div className="row">
                <div className="col-lg-4">
                  <h2>Important dates</h2>
                  <dl className="dl-horizontal">
                    <dt>Game start:</dt>
                    <dd>{startTime.toLocaleTimeString()}<br/>({startTime.toDateString()})</dd>
                    <dt>Game end:</dt>
                    <dd>{endTime.toLocaleTimeString()}<br/>({endTime.toDateString()})</dd>
                  </dl>
                </div>
                <div className="col-lg-4">
                  <h2>Game status</h2>
                  <h4>{game.gameState.split(/(?=[A-Z])/).join(" ")}</h4>
                </div>
                <div className="col-lg-4">
                  <h2>Players</h2>
                  <dl className="dl-horizontal">
                    <dt>Total players:</dt>
                    <dd>{humans + zombies}</dd>
                    <dt>Humans:</dt>
                    <dd>{humans}</dd>
                    <dt>Zombies:</dt>
                    <dd>{zombies}</dd>
                    <dt>Squads:</dt>
                    <dd>{squads}</dd>
                  </dl>
                </div>
              </div>
              <br/>
          <GameDetailRegistration
            game={game}
            player={player}
            addRegisteredPlayer={this.addRegisteredPlayer.bind(this)}
          ></GameDetailRegistration>
          <GameStart game={game} player={player} start={g => this.setState({ game: g })}></GameStart>
          <BiteCode game={game} player={player} fetchKills={this.fetchKills.bind(this)}></BiteCode>
          <SquadRegistration game={game} player={player} addSquad={this.addSquad.bind(this)}></SquadRegistration>
          <SquadDetails
            game={game}
            player={player}
            squads={this.state.squads}
            addSquadMember={this.addSquadMember.bind(this)}
            removeSquadMember={this.removeSquadMember.bind(this)}>
          </SquadDetails>
          <br />

          {player.playerName ? (
            <div id="chat-container">
              <AllChat gameId={game.id} player={player}></AllChat>
              {this.state.player
                .isHuman /* Only render the correct faction-chat */ ? (
                <HumanChat gameId={game.id} player={player}></HumanChat>
              ) : (
                <ZombieChat gameId={game.id} player={player}></ZombieChat>
              )}
            </div>
          ) : (
            ""
          )}

          <br />
          <div id="container">
            <div id="content">
            <Map style={mapStyles} game={this.state.game} markers={this.state.kills} iconUrl={"https://i.imgflip.com/2x8vx1.png"}></Map>
            </div>
          </div>
        </div>
      );
    } else if (this.state.loading === false) {
      return <div>Loading...</div>;
    }
  }
}

export default GameDetail;

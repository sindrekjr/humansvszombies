import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import authService from "../api-authorization/AuthorizeService";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import Card from "react-bootstrap/Card";
import Jumbotron from "react-bootstrap/Jumbotron";

/* Registration Fragment. A simple button that allows a user to register as a
player in this game. */
/* Lets the current user register as participant of the game as part of the human "squad"?*/

let user;
let match;

class GameDetailRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gameid: props.game.id,
      game: props.game,
      isPlayer: true,
      players: []
    };
  }

  state = {
    players: [],
    gameid: null,
    player: {},
    game: {},
    user: {},
    isPlayer: true,
    userName: null
  };

  async componentDidMount() {
    try {
      const response = await fetch("api/game/" + this.state.gameid + "/player");
      const json = await response.json();

      await this.setState({
        players: json
      });
    } catch (e) {
      console.error(e);
    }

    user = await authService.getUser();

    if(user) {
      let players = this.state.players;
      match = players.find(player => player.userId === user.sub);
      if (match === undefined) {
        this.setState({ isPlayer: false });
      }
      await this.setState({ user: user, userName: user.name });
    }    
  }

  async createPlayer(e) {
    let bitecode = Math.random()
      .toString(36)
      .substring(7);
    e.preventDefault();
    let userid = user.sub;
    await this.setState({
      player: {
        userId: userid,
        playerName: this.inputPlayerName.value,
        isHuman: true,
        isPatientZero: false,
        biteCode: bitecode,
        gameId: this.state.gameid
      }
    });
    this.postPlayer();
  }

  async postPlayer() {
    let id = this.state.gameid;
    let url = "/api/game/" + id + "/player";
    const token = await authService.getAccessToken();

    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token
        },
        body: JSON.stringify(this.state.player)
      });
      const registeredPlayer = await response.json();
      this.props.addRegisteredPlayer(registeredPlayer)
      this.setState({
        isPlayer: true
      })
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    if(!this.state.user) {
      return <Card.Header as="h4">You will need to register to enter the game.</Card.Header>;
    } else if (
      this.state.isPlayer !== true &&
      this.state.game.gameState === "Registration"
    ) {
      return (
        <div>
          <Card bg="dark" border="secondary">
            <Card.Header as="h4">
              Register as a player in this game?
            </Card.Header>
            <Card.Body>
              <Form onSubmit={this.createPlayer.bind(this)}>
                <Form.Group as={Row} controlId="formPlaintextEmail">
                  <Form.Label column sm="2">
                    Email
                  </Form.Label>
                  <Col sm="10">
                    <Form.Control
                      type="text"
                      readOnly
                      defaultValue={this.state.userName}
                    />
                  </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="formUserName">
                  <Form.Label column sm="2">
                    Username
                  </Form.Label>
                  <Col sm="10">
                    <Form.Control
                      type="text"
                      placeholder="Choose a username for the game"
                      required
                      ref={input => (this.inputPlayerName = input)}
                    />
                  </Col>
                </Form.Group>

                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </div>
      );
    } else if (
      this.state.game.gameState === "InProgress" &&
      this.state.isPlayer !== true
    ) {
      return (
        <Alert variant="warning">
          This game has already started and registration is closed
        </Alert>
      );
    } else if (
      this.state.game.gameState === "Complete" &&
      this.state.isPlayer !== true
    ) {
      return <Alert variant="danger">This game has finished.</Alert>;
    } else {
      return (
        <Jumbotron className="jumbo">
          <div>
            <h5>You are registered for this game</h5>
            <h5>Player name: {this.props.player.playerName}</h5>
            <Status player={this.props.player}></Status>
            <IsAdmin player={this.props.player}></IsAdmin>
            <br />
          </div>
        </Jumbotron>
      );
    }
  }
}

export default GameDetailRegistration;

function Status(props) {
  const human = props.player.isHuman;
  if (human === true) {
    return <h5>Status: Human</h5>;
  } else if (human === false) {
    return <h5>Status: Zombie</h5>;
  } else return null;
}
function IsAdmin(props) {
  if (props.player.isGameAdmin === true) {
    return <h5> You are the game admin</h5>;
  } else return null;
}

import React from "react";
import Card from "react-bootstrap/Card";

/* Title Fragment. The name, description and rules of the game should be displayed
to any user. The name and description should vary between games but the rules
should be constant. */



const GameDetailTitle = props => {

  return (
    <div>
    <h1>{props.game.name}</h1>
    <br/>
    <Card bg="dark" border="secondary">
      <Card.Header as="h4">Rules and description</Card.Header>
      <Card.Body>
        <p>
          The storyline of the game dictates that players begin as Humans and try to survive a Zombie invasion.
        </p>
        <p>
          A zombie must tag a human to kill them.
        </p>
        <p>
          Upon the start of the game, every player is a Human and only one Zombie exists. 
          When tagged by a Zombie, a Human is considered "dead" (or technically "undead") 
          and becomes a part of the Zombie team. 
        </p>
        <p>
          Zombies try to turn all the Humans by tagging them. 
          Humans try to survive for the entire duration of the game.
        </p>
      </Card.Body>
    </Card>
    <br></br>
    </div>
  )
}

export default GameDetailTitle
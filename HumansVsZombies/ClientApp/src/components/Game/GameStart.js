import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import authService from "../api-authorization/AuthorizeService";

class GameStart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gameState: props.game.gameState
    };
  }
  async startGame(e) {
    e.preventDefault();
    const token = await authService.getAccessToken();
    let players = this.props.game.players;
    var randomPlayer = players[Math.floor(Math.random() * players.length)];

    const url = "api/game/" + this.props.game.id + "/player/" + randomPlayer.id;
    const data = {
      id: randomPlayer.id,
      gameId: this.props.game.id,
      playerName: randomPlayer.playerName,
      userId: randomPlayer.userId,
      isHuman: false,
      isPatientZero: true,
      isGameAdmin: randomPlayer.isGameAdmin,
      biteCode: randomPlayer.biteCode
    };

    await fetch(url, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: JSON.stringify(data)
    });

    this.putGame(token).then(() => {
      this.setState({ gameState: "InProgress" });
      let game = this.props.game;
      game.gameState = "InProgress";
      this.props.start(game);
      alert("The game has started. Good luck!")
    });
  }

  async putGame(token) {
    const url = "api/game/" + this.props.game.id;
    const data = {
      id: this.props.game.id,
      name: this.props.game.name,
      gameState: "InProgress",
      NELat: this.props.game.neLat, 
      NELng: this.props.game.neLng, 
      SWLat: this.props.game.swLat, 
      SWLng: this.props.game.swLng
    };

    return fetch(url, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: JSON.stringify(data)
    });
  }

  render() {
    if (
      this.props.player.isGameAdmin === true &&
      this.state.gameState === "Registration"
    ) {
      return (
        <div>
          <Button
            variant="primary"
            size="lg"
            block
            onClick={this.startGame.bind(this)}
          >
            Start the game!
          </Button>
          <br />
        </div>
      );
    } else return null;
  }
}

export default GameStart;

import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Accordion from "react-bootstrap/Accordion";
import ListGroup from "react-bootstrap/ListGroup";
import authService from "../api-authorization/AuthorizeService";

/* Squad Details Fragment. This should display the names, relative ranks and
state of each of the members of your squad. Additionally there should be buttons
to leave a check in marker on your current position and to leave the squad. */

class SquadDetails extends Component {
  constructor(props) {
    super(props);
    this.state = { squads: [], player: props.player, gameid: props.game.id };
  }

  state = {
    squads: [],
    squad: {},
  };

  addSquadMemeber = async (newSquadMember) => {
    this.props.addSquadMember(newSquadMember)
  }

  render() {
    const squads = Array.from(this.props.squads).map(squad => { 
      return <Squads squad={squad} key={squad.id} player={this.props.player} addSquadMemeber={this.addSquadMemeber.bind(this)}  removeSquadMember={this.props.removeSquadMember}/>;
    });
    return (
      <div>
        <br />
        <h3>Squads</h3>
        {squads}
      </div>
    );
  }
}

class SquadJoinLeave extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sqaudMember: {}
    };
  }

  async joinSquad(e) {
    e.preventDefault();
    const player = this.props.player;
    const squad = this.props.squad;
    if (player.isHuman === true && squad.isHuman === false) {
      alert("You can't join a Zombie squad as a Human");
    } else if (player.isHuman === false && squad.isHuman === true) {
      alert("You can't join a Human squad as a Zombie");
    } else {
      await this.setState({
        squadMember: {
          playerId: player.id,
          playerName: player.playerName,
          squadId: this.props.squad.id,
          gameId: player.gameId,
          rank: "Member"
        }
      });
      this.postSquadMember();
    }
  }

  async postSquadMember() {
    let id = this.props.player.gameId;
    let squadid = this.props.squad.id;
    let url = "/api/game/" + id + "/squad/" + squadid + "/join";
    const token = await authService.getAccessToken();

    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token
        },
        body: JSON.stringify(this.state.squadMember)
      });
      const newSquadMember = await response.json();
      this.props.addSquadMemeber(newSquadMember)
    } catch (e) {
      console.error(e);
    }
  }

  async leaveSquad(e) {
    e.preventDefault();
    let id = this.props.player.gameId;
    let squadid = this.props.squad.id;

    const token = await authService.getAccessToken();
    const url = "api/game/" + id + "/squad/" + squadid + "/leave";

    try {
      const response = await fetch(url, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token
        }
      });
      const deletedSquadMember = await response.json();
      this.props.removeSquadMember(deletedSquadMember)
    } catch (e) {
      console.error(e);
    }
  }
  render() {
    let squad = this.props.squad.squadMembers;
    let player = this.props.player;

    let match = squad.find(squadMember => {
      if(squadMember === undefined){
        return undefined
      }
      return squadMember.playerId === player.id
    });

    if (match === undefined) {
      return (
        <div>
          <Button variant="success" onClick={this.joinSquad.bind(this)}>
            Join Squad
          </Button>
        </div>
      );
    } else
      return (
        <div>
          <Button variant="danger" onClick={this.leaveSquad.bind(this)}>
            Leave Squad
          </Button>
        </div>
      );
  }
}

const Squads = props => {
  const addSquadMemeber = (newSquadMember) => {
    props.addSquadMemeber(newSquadMember)
  }

  let status = "";
  if (props.squad.isHuman === true) {
    status = "Human";
  }
  if (props.squad.isHuman === false) {
    status = "Zombie";
  }

  let squadMembers = props.squad.squadMembers.map((squadMembers, i) => (
    <ListGroup horizontal key={i}>
      <ListGroup.Item variant="dark">
        <b>{squadMembers.playerName}</b>
        <br />
        Rank: {squadMembers.rank}
      </ListGroup.Item>
    </ListGroup>
  ));
  if (props.player.id !== undefined){
    return (
      <div>
        <Accordion>
          <Card bg="dark" border="secondary">
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="0">
                {props.squad.name}
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="0">
              
              <Card.Body>
                <h4>{props.squad.squadMembers.length} Member(s):</h4>
                {squadMembers}
                <br />
                <h4>Status: {status}</h4>
                <SquadJoinLeave squad={props.squad} player={props.player} addSquadMemeber={addSquadMemeber.bind(this)} removeSquadMember={props.removeSquadMember}></SquadJoinLeave>
                {props.player.squadMembers === null || props.player.squadMembers.filter(m => m.squadId === props.squad.id).length ?
                  <Link to={
                    {
                      pathname: `/game/${props.squad.gameId}/squad/${props.squad.id}`,
                      state: {
                        game: props.game,
                        player: props.player,
                        squad: props.squad
                      }
                    }}>
                      <Button>Enter</Button>
                  </Link>
                  : ""
                }
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
      </div>
    )
  } else {
    return (<div><ListGroup>
      <ListGroup.Item variant="dark">{props.squad.name}
      </ListGroup.Item>
    </ListGroup></div>)
  }
};
export default SquadDetails;

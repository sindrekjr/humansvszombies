import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import authService from "../api-authorization/AuthorizeService";

/* Squad Registration Fragment. The squad registration fragment should display
a small form to create a new squad and a list of available squads to be joined. Each
entry in the list should display thier name, respective total number of members,
number of "deceased" members and a button to join the squad. */

class SquadRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      game: props.game,
      player: props.player,
      squad: {}
    };
  }

  
  async addSquad(e) {
    e.preventDefault();
    await this.setState({
      squad: {
        name: this.inputSquadName.value,
        gameId: this.state.game.id,
        isHuman: this.props.player.isHuman
      }
    });
    this.createSquad();
  }

  async createSquad() {
    const token = await authService.getAccessToken();
    const url = "api/game/" + this.state.game.id + "/squad";
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token
        },
        body: JSON.stringify(this.state.squad)
      });
      const newSquad = await response.json();
      this.props.addSquad(newSquad);
      document.getElementById("squad-registration-form").reset();
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    if (this.props.player.id !== undefined && this.state.game.gameState !== "Complete"){
    return (
      <div>
      <br/>
      <Card bg="dark" border="secondary">
      <Card.Header>Create a new Squad</Card.Header>
      <Card.Body>
        <Form id="squad-registration-form" onSubmit={this.addSquad.bind(this)}>
          <Form.Group controlId="formBiteCode">
            <Form.Label>Squad name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter squad name"
              required
              ref={input => (this.inputSquadName = input)}
            />
             <Form.Text className="text-muted">
                Create a new squad with yourself as the leader.
              </Form.Text>
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
        </Card.Body>
      </Card>
      </div>
    );
  }
  else return null
}
}

export default SquadRegistration;

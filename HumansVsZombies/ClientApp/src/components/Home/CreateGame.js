import React, { Component } from "react";
import authService from "../api-authorization/AuthorizeService";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

class CreateGame extends Component {
  state = {
    token: null,
    game: {},
    playerName: ""
  };
  async componentDidMount() {
    const token = await authService.getAccessToken();
    await this.setState({ token: token });
  }

  async addGame(e) {
    e.preventDefault();

    let startTime = new Date();
    let endTime = new Date();

    startTime.setDate(startTime.getDate() + 7);
    endTime.setDate(endTime.getDate() + 8);

    console.log(startTime);

    await this.setState({
      game: {
        name: this.inputGameName.value,
        gameState: 0,
        startTime: startTime,
        endTime: endTime,
        NELat: 60.399819, 
        NELng: 5.355332, 
        SWLat: 60.380307, 
        SWLng: 5.295871
      },
      playerName: this.inputPlayerName.value
    });
    this.createGame();
  }

  async createGame() {
    const url = "api/game";
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + this.state.token
        },
        body: JSON.stringify(this.state.game)
      });
      this.renameAdmin(await response.json()).then(() => {
        this.props.fetchAgain();
        document.getElementById("create-game-form").reset();
      });
    } catch (e) {
      console.error(e);
    }
  }

  async renameAdmin(game) {
    // Get admin
    let url = "api/game/" + game.id + "/player/me";
    try {
      const response = await fetch(url,{
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + this.state.token
        }
      });
      let player = await response.json();
      // change the name
      player.playerName = this.state.playerName
      url = "api/game/" + game.id + "/player/" + player.id
      return fetch(url, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + this.state.token
        },
        body: JSON.stringify(player)
      });
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    if (this.state.token != null) {
      return (
        <div>
          <br />
          <Card bg="dark" border="secondary">
            <Card.Header>Create a new game</Card.Header>
            <Card.Body>
              <Form id="create-game-form" onSubmit={this.addGame.bind(this)}>
                <Form.Group controlId="formAddGame">
                  <Form.Label>Game name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter game name.."
                    required
                    ref={input => (this.inputGameName = input)}
                    
                  />
                  <Form.Label>Player Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter your player name.."
                    required
                    ref={input => (this.inputPlayerName = input)}
                  />
                  <Form.Text className="text-muted">
                    Create a new game with yourself as the game administrator.
                  </Form.Text>
                </Form.Group>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </div>
      );
    } else {
      return (
        <div>
          <br />
          <p>Register or log in to create a new game</p>
        </div>
      )
    }
  }
}

export default CreateGame;

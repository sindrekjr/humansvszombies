import React, { Component } from "react";
import Container from "react-bootstrap/Container";

import './GameList.css';

import Games from "./Games";

class GameList extends Component {
   
    state = {
        game: {}
    }

  render() {
      const games = Array.from(this.props.games).map(game => {
          return <Games game={game} key={game.id} />
      
      });
    return (
    <div>
    <div className="container">
          <Container fluid>
            <div className="games" id="gamesId">{games}</div>
          </Container>
        </div>
    </div>
    );
  }
}

export default GameList;
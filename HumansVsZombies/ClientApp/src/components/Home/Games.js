import React from "react";
import Card from "react-bootstrap/Card";
import CardDeck from "react-bootstrap/CardDeck";
import { Link } from "react-router-dom";

import './Games.css';

const Games = props => {
    return (
        <CardDeck className="card-deck">
            <Card bg="dark" border="secondary" className="card">
            <div className="card-body">
                <Link to={{
                    pathname: "/game/" + props.game.id,
                    state: {
                    game: props.game
                    }
                }}>
                  <Card.Body>
                    <Card.Title>{props.game.name}</Card.Title>
                    <Card.Text>Game state: {props.game.gameState.split(/(?=[A-Z])/).join(" ")}</Card.Text>
                    <Card.Text>Players: {props.game.players.length}</Card.Text>
                    </Card.Body>
              </Link>
            </div>
            </Card>
        </CardDeck>
    )
}

export default Games; 
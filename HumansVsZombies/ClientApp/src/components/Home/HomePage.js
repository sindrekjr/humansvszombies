import React, { Component } from "react";
//import "./HomePage.css"
import GameList from "./GameList";
import CreateGame from "./CreateGame";

const baseUrl = "api/game"

class HomePage extends Component {

  state = {
    games: {}
  }

  async componentDidMount() {
    this.gameFetch()
  }

  async fetchAgain(){
    await this.gameFetch()
  }

  async gameFetch() {
    try {
        const response = await fetch(baseUrl)
        const json = await response.json();
        
        await this.setState({
            games: json,
        });
    } catch(e) {
        console.error(e)
    }
  }

  render() {
    return (
      <div>
        <h1>Game List</h1>
        <div className="games">
          <GameList games={this.state.games}></GameList>
          </div>
          <CreateGame fetchAgain={this.fetchAgain.bind(this)}></CreateGame>
      </div>
    );
  }
}
export default HomePage;

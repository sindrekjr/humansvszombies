import React, { Component } from 'react';
import { GoogleApiWrapper, InfoWindow, Marker, Map } from 'google-maps-react';

import './Map.css';

class MapComponent extends Component {
  state = {
    loading: true,
    bounds: null,
    markers: [],
    players: [],
    style: {},
    icon: null,
    activeMarker: {},
    selectedPlace: {},
    showingInfoWindow: false
  }

  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });

  onClose = props => {
    if(this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  };

  onMapClicked = () => {
    if(this.state.showingInfoWindow)
      this.setState({
        activeMarker: null,
        showingInfoWindow: false
      });
  };

  componentDidMount() {
    fetch(`api/game/${this.props.game.id}/player`).then(response => response.json()).then(players => {
      this.setState({
        bounds: this.props.bounds || this.getBounds(this.props.game),
        markers: this.props.markers || [],
        players: players,
        style: Object.assign({
          height: '400px',
          width: '400px'
        }, this.props.style),
        icon: this.props.iconUrl ? new this.props.google.maps.MarkerImage(
          this.props.iconUrl, 
          null, 
          null,
          null,
          new this.props.google.maps.Size(32, 32)
        ) : null,
        loading: false
      });
    })
  }

  getBounds(game) {
    const {
      swLat, 
      swLng,
      neLat,
      neLng
    } = game;

    let bounds;
    if(swLat && swLng && neLat && neLng) {
      bounds = new this.props.google.maps.LatLngBounds(
        {
          lat: swLat,
          lng: swLng
        },
        {
          lat: neLat,
          lng: neLng
        }
      );
    } else {
      bounds = new this.props.google.maps.LatLngBounds();
      this.state.markers.forEach(marker => {
        bounds.extend({ lat: marker.lat, lng: marker.lng });
      })
    }

    return bounds;
  }

  render() {
    return <div className='Map-wrapper' style={this.state.style}>
      {this.state.loading
        ? <Map google={this.props.google}></Map>
        : <Map google={this.props.google}
            bounds={this.state.bounds}
          >
            {this.renderMarkers()}
            <InfoWindow marker={this.state.activeMarker} visible={this.state.showingInfoWindow}>
              <p>{this.parseInfoWindowFromMarkerData()}</p>
            </InfoWindow>
          </Map>
      }
    </div>
  }

  renderMarkers() {
    let renderedMarkers = [];

    this.props.markers.forEach(marker => {
      renderedMarkers.push(
        <Marker
          key={marker.id}
          markerId={marker.id}
          position={{
            lat: marker.lat,
            lng: marker.lng
          }}
          onClick={this.onMarkerClick}
          icon={this.state.icon}
        />
      )
    });

    return renderedMarkers;
  }

  parseInfoWindowFromMarkerData() {
    if(this.state.selectedPlace.markerId) {
      const markerData = this.props.markers.find(m => m.id === this.state.selectedPlace.markerId);

      if(markerData.killerId && markerData.victimId) {
        const timeOfDeath = new Date(markerData.timeOfDeath);

        return <div>
          <h6>Kill Location</h6>
          <p>
            Killer: {this.state.players.find(p => p.id === markerData.killerId).playerName}<br/>
            Victim: {this.state.players.find(p => p.id === markerData.victimId).playerName}<br/>
            
          </p>
          <p>
            Time of Death:<br/>
            {timeOfDeath.toLocaleTimeString()}, {timeOfDeath.toDateString()}
          </p>
          <p>
            {markerData.story}
          </p>
        </div>;
      } else if(markerData.squadId && markerData.squadMemberId) {
        return <div>
          <h6>Check-in</h6>
          <p>This is just a squad check-in from {new Date(markerData.startTime).toDateString()}.</p>
        </div>
      } else if(markerData.description) {
        return <div>
          <h6>Mission</h6>
          <p>{markerData.description}</p>
        </div>
      }      
    }

    return null;
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyAO-qJhf0DqbrnBt7dmtrFNMCkJPM01oPc'
})(MapComponent);
import React, { Component } from 'react';
import Button from "react-bootstrap/Button";
import authService from "../api-authorization/AuthorizeService";

import './SquadHome.css';
import Map from './../Map/Map';
import SquadChat from '../Chat/SquadChat';

class SquadHome extends Component {
  state = {
    loading: true,
    squad: null,
    player: null,
    game: null
  }

  componentDidMount() {
    this.initialize(true);
  }

  initialize(useInitialValues = false) {
    let squad, game, player;
    if(useInitialValues) {
      squad = this.props.location.state.squad;
      game = this.props.location.state.game;
      player = this.props.location.state.player;
    }

    if(squad && game && player) {
      this.setState({
        loading: false,
        squad: squad,
        game: game,
        player: player
      });
    } else {
      this.fetchSquad().then(squad => this.setState({
        loading: false,
        squad: squad
      }));
  
      this.fetchGame().then(game => this.setState({
        game: game
      }));
    
      this.fetchPlayer().then(player => this.setState({
        player: player
      }));
    }
  }

  async fetchSquad() {
    return fetch(`api/game/${this.props.match.params.gameId}/squad/${this.props.match.params.id}`).then(response => response.json());
  }

  async fetchGame() {
    return fetch(`api/game/${this.props.match.params.gameId}`).then(response => response.json());
  }

  async fetchPlayer() {
    const accessToken = await authService.getAccessToken();

    return fetch(
      `api/game/${this.props.match.params.gameId}/player/me`,
      {
        'method': 'GET',
        'headers': {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${accessToken}`
        }
      }
    ).then(response => response.json());
  }

  async checkIn() {
    const accessToken = await authService.getAccessToken();

    navigator.geolocation.getCurrentPosition(pos => {
      const coordinates = pos.coords;

      let checkIn = {
        gameId: this.state.game.id,
        squadId: this.state.squad.id,
        squadMemberId: this.state.squad.squadMembers.find(member => member.playerId === this.state.player.id).id,
        startTime: new Date(),
        endTime: new Date(),
        lat: coordinates.latitude,
        lng: coordinates.longitude
      }

      fetch(
        `api/game/${this.props.match.params.gameId}/squad/${this.props.match.params.id}/check-in`,
        {
          'method': 'POST',
          'headers': {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
          },
          'body': JSON.stringify(checkIn)
        }
      ).then(() => {
        this.initialize();
      });
    });
  }

  render() {
    return this.state.loading ? <h2>Loading...</h2> : <div className='SquadHome'>
      <h2>{this.state.squad.name}</h2>
      <div className='SquadHome-main'>
        <article className='SquadHome-left'>
          {this.renderMemberList()}
          {this.renderChat()}
        </article>
        <aside className='SquadHome-right'>
          {this.renderMap()}
          <Button variant="success" onClick={() => this.checkIn()}>
            Check In
          </Button>
      </aside>
      </div>
    </div>
  }

  renderMemberList() {
    return <ul className='SquadHome-memberlist'>
      {this.state.squad.squadMembers.map(member => <li key={member.id}>{member.playerName}</li>)}
    </ul>;
  }

  renderChat() {
    if(!this.state.player) {
      return <h4>Loading chat...</h4>
    } else {
      return <SquadChat player={this.state.player} gameId={this.state.squad.gameId} squadId={this.state.squad.id}></SquadChat>;
    }
  }

  renderMap() {
    if(this.state.game && this.state.squad.squadCheckIns) {
      return <Map style={{height: '500px', width: '600px'}} game={this.state.game} markers={this.state.squad.squadCheckIns}></Map>
    } else {
      return null;
    }
  }
}

export default SquadHome;

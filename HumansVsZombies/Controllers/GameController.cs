﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using HumansVsZombies.Data;
using HumansVsZombies.Models;


namespace HumansVsZombies.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GameController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public GameController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Game
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<Game>>> GetGames()
        {
            return await _context.Games.ToListAsync();
        }

        // GET: api/Game/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<Game>> GetGame(int id)
        {
            var game = await _context.Games.FindAsync(id);

            if (game == null)
            {
                return NotFound();
            }

            return game;
        }

        // PUT: api/Game/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGame(int id, Game game)
        {
            if (id != game.Id) return BadRequest("Error: Mismatched game id and game object.");

            // Find current user and verify that it's not null
            User currentUser = await GetLoggedInUser();
            if (currentUser == null) return Unauthorized();

            // Find current player and verify that it's not null
            Player currentPlayer = await GetLoggedInPlayer(currentUser, id);
            if (currentPlayer == null || !currentPlayer.IsGameAdmin || currentPlayer.GameId != id) return Forbid();

            _context.Entry(game).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Game
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Game>> PostGame(Game game)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[6];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            // Find current user and verify that it's not null
            User currentUser = await GetLoggedInUser();
            if (currentUser == null) return Unauthorized();

            // Add the new game object and save changes
            _context.Games.Add(game);
            await _context.SaveChangesAsync();

            // Create player object or the current user with admin privilege
            Player player = new Player { UserId = currentUser.Id, GameId = game.Id, PlayerName = $"Game{game.Id}Creator", IsGameAdmin = true, IsHuman = true, BiteCode= finalString};
            _context.Players.Add(player);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGame", new { id = game.Id }, game);
        }

        // DELETE: api/Game/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Game>> DeleteGame(int id)
        {
            // Find current user and verify that it's not null
            User currentUser = await GetLoggedInUser();
            if (currentUser == null) return Unauthorized();

            // Find current player and verify that it's not null
            Player currentPlayer = await GetLoggedInPlayer(currentUser, id);
            if (currentPlayer == null || !currentPlayer.IsGameAdmin || currentPlayer.GameId != id) return Forbid();

            var game = await _context.Games.FindAsync(id);
            if (game == null)
            {
                return NotFound();
            }

            if (!currentPlayer.IsGameAdmin)
            {
                return Unauthorized("Only game admins can delete a game.");
            }

            _context.Games.Remove(game);
            await _context.SaveChangesAsync();

            return game;
        }

        // GET: api/Game/me
        // Returns the current logged in users id
        [HttpGet("me")]
        public async Task<ActionResult<string>> GetCurrentUserId()
        {
            User currentUser = await GetLoggedInUser();
            return currentUser.Id;

        }



        private bool GameExists(int id) => _context.Games.Any(e => e.Id == id);

        private async Task<User> GetLoggedInUser() => await _context.Users.FindAsync(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);

        private async Task<Player> GetLoggedInPlayer(User currentUser, int gameId) => await _context.Players.Where(p => p.UserId == currentUser.Id && p.GameId == gameId).FirstOrDefaultAsync();
    }
}

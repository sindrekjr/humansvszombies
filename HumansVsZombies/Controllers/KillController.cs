﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using HumansVsZombies.Data;
using HumansVsZombies.Models;


namespace HumansVsZombies.Controllers
{
    [Route("api/game/{gameId}/[controller]")]
    [ApiController]
    [Authorize]
    public class KillController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public KillController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/game/1/Kill
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<Kill>>> GetKills(int gameId)
        {
            var game = await _context.Games.FindAsync(gameId);

            if (game == null)
            {
                return NotFound();
            }

            return await _context.Kills.Where(k => k.GameId == gameId).ToListAsync();
        }

        // GET: api/game/1/Kill/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<Kill>> GetKill(int id, int gameId)
        {
            var kill = await _context.Kills.FindAsync(id);

            if (kill == null || kill.GameId != gameId)
            {
                return NotFound();
            }

            return kill;
        }

        // PUT: api/game/1/Kill/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutKill(int id, Kill kill, int gameId)
        {
            if (id != kill.Id)
            {
                return BadRequest("Error: Mismatched kill id and kill object.");
            }
            else if (kill.GameId != gameId)
            {
                return NotFound();
            }

            // Check if admin or killer
            User currentUser = await GetLoggedInUser();

            if (currentUser == null)
            {
                return Unauthorized();
            }

            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            if (currentPlayer == null || (!currentPlayer.IsGameAdmin && currentPlayer.Id != kill.KillerId) || currentPlayer.GameId != gameId)
            {
                return Forbid();
            }
            // Admin check done.

            _context.Entry(kill).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KillExists(id, gameId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/game/1/Kill
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Kill>> PostKill(Kill kill, int gameId)
        {
            if (kill.GameId != gameId)
            {
                return NotFound();
            }

            User currentUser = await GetLoggedInUser();

            if (currentUser == null)
            {
                return Unauthorized();
            }

            Player killer = await _context.Players.FindAsync(kill.KillerId);
            Player victim = await _context.Players.Where(p => p.BiteCode == kill.BiteCode).FirstOrDefaultAsync();

            if (victim == null)
            {
                return BadRequest("Error: Failed to resolve victim from bite code.");
            }

            kill.VictimId = victim.Id;

            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            if (killer.Id != currentPlayer.Id)
            {
                return BadRequest("Error: Mismatched player login and killer object.");
            }


            if (killer.GameId != gameId || killer.GameId != victim.GameId)
            {
                return BadRequest("Error: Can't kill from another game!");
            } 
            else if (killer.IsHuman || !victim.IsHuman) 
            {
                return BadRequest("Error: A zombie needs to infect a human!");
            }

            victim.IsHuman = false;
            _context.Kills.Add(kill);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetKill", new { id = kill.Id }, kill);
        }

        // DELETE: api/game/1/Kill/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Kill>> DeleteKill(int id, int gameId)
        {
            var kill = await _context.Kills.FindAsync(id);
            if (kill == null || kill.GameId != gameId)
            {
                return NotFound();
            }
            
            // Check if admin
            User currentUser = await GetLoggedInUser();
            if (currentUser == null)
            {
                return Unauthorized();
            }

            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);
            if (currentPlayer == null || !currentPlayer.IsGameAdmin || currentPlayer.GameId != gameId)
            {
                return Forbid();
            }
            // Admin check done.

            _context.Kills.Remove(kill);
            await _context.SaveChangesAsync();

            return kill;
        }

        private bool KillExists(int id, int gameId)
        {
            return _context.Kills.Any(e => e.Id == id && e.GameId == gameId);
        }
        private async Task<User> GetLoggedInUser()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            return await _context.Users.FindAsync(userId);
        }

        private async Task<Player> GetLoggedInPlayer(User currentUser, int gameId)
        {
            return await _context.Players.Where(p => p.UserId == currentUser.Id && p.GameId == gameId).FirstOrDefaultAsync();
        }
    }
}

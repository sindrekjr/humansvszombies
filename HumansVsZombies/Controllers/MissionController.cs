﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using HumansVsZombies.Data;
using HumansVsZombies.Models;
using System.Security.Claims;

namespace HumansVsZombies.Controllers
{
    [Route("api/game/{gameId}/[controller]")]
    [ApiController]
    [Authorize]
    public class MissionController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public MissionController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/game/1/Mission
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Mission>>> GetMissions(int gameId)
        {
            var game = await _context.Games.FindAsync(gameId);

            if (game == null)
            {
                return NotFound();
            }

            User currentUser = await GetLoggedInUser();
            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            if (currentPlayer == null)
            {
                return Forbid();
            }

            // Returns mission from correct game and to the proper factions.
            return await _context.Missions.Where(m => m.GameId == gameId && ((m.IsHumanVisible && currentPlayer.IsHuman) || (m.IsZombieVisible && !currentPlayer.IsHuman))).ToListAsync();
        }

        // GET: api/game/1/Mission/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Mission>> GetMission(int id, int gameId)
        {
            var mission = await _context.Missions.FindAsync(id);

            if (mission == null || mission.GameId != gameId)
            {
                return NotFound();
            }

            User currentUser = await GetLoggedInUser();
            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            if ((mission.IsHumanVisible && currentPlayer.IsHuman) || (mission.IsZombieVisible && !currentPlayer.IsHuman))
            {
                return mission;
            }

            // The player is not in the correct faction to view this mission.
            return Forbid();
        }

        // PUT: api/game/1/Mission/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMission(int id, Mission mission, int gameId)
        {
            if (id != mission.Id)
            {
                return BadRequest();
            }
            else if (mission.GameId != gameId)
            {
                return NotFound();
            }

            // Check if admin
            User currentUser = await GetLoggedInUser();

            if (currentUser == null)
            {
                return Forbid();
            }

            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            if (currentPlayer == null || !currentPlayer.IsGameAdmin || currentPlayer.GameId != gameId)
            {
                return Forbid();
            }
            // Admin check done.

            _context.Entry(mission).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MissionExists(id, gameId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/game/1/Mission
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Mission>> PostMission(Mission mission, int gameId)
        {
            if (mission.GameId != gameId)
            {
                return NotFound();
            }
            _context.Missions.Add(mission);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMission", new { id = mission.Id }, mission);
        }

        // DELETE: api/game/1/Mission/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Mission>> DeleteMission(int id, int gameId)
        {
            var mission = await _context.Missions.FindAsync(id);
            if (mission == null || mission.GameId != gameId)
            {
                return NotFound();
            }

            // Check if admin
            User currentUser = await GetLoggedInUser();

            if (currentUser == null)
            {
                return Forbid();
            }

            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            if (currentPlayer == null || !currentPlayer.IsGameAdmin || currentPlayer.GameId != gameId)
            {
                return Forbid();
            }
            // Admin check done.

            _context.Missions.Remove(mission);
            await _context.SaveChangesAsync();

            return mission;
        }

        private bool MissionExists(int id, int gameId)
        {
            return _context.Missions.Any(e => e.Id == id && e.GameId == gameId);
        }

        private async Task<User> GetLoggedInUser()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            return await _context.Users.FindAsync(userId);
        }

        private async Task<Player> GetLoggedInPlayer(User currentUser, int gameId)
        {
            return await _context.Players.Where(p => p.UserId == currentUser.Id && p.GameId == gameId).FirstOrDefaultAsync();
        }
    }
}

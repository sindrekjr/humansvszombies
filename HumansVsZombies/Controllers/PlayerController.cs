﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using HumansVsZombies.Data;
using HumansVsZombies.Models;


namespace HumansVsZombies.Controllers
{
    [Route("api/game/{gameid}/[controller]")]
    [ApiController]
    [Authorize]
    public class PlayerController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public PlayerController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/game/1/Player
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<Player>>> GetPlayers(int gameId)
        {
            if (!GameExists(gameId))
            {
                return BadRequest();
            }
            Game currentGame = await _context.Games.FindAsync(gameId);

            User currentUser = await GetLoggedInUser();

            //Not logged in gets partial player objects.
            if (currentUser == null)
            {
                return await _context.Players.Select(p => new Player() { Id = p.Id, Death = p.Death, GameId = p.GameId, IsHuman = p.IsHuman, Kills = p.Kills, PlayerName = p.PlayerName, UserId = p.UserId })
                                        .Where(p => p.GameId == gameId).ToListAsync();
            }

            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            //Non admin gets partial player objects
            if (currentPlayer == null || !PlayerIsAdminOfGame(currentPlayer, currentGame))
            {
                return await _context.Players.Select(p => new Player() { Id = p.Id, Death = p.Death, GameId = p.GameId, IsHuman = p.IsHuman, Kills = p.Kills, PlayerName = p.PlayerName, UserId = p.UserId })
                                        .Where(p => p.GameId == gameId).ToListAsync();
            }

            return await _context.Players.Where(p => p.GameId == gameId).ToListAsync();
        }

        // GET: api/game/1/Player/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<Player>> GetPlayer(int id, int gameId)
        {
            var requestedPlayer = await _context.Players.FindAsync(id);

            if (!GameExists(gameId) || requestedPlayer == null || requestedPlayer.GameId != gameId)
            {
                return BadRequest();
            }
            Game currentGame = await _context.Games.FindAsync(gameId);

            User currentUser = await GetLoggedInUser();

            if(currentUser == null)
            {
                return new Player() { Id = requestedPlayer.Id, Death = requestedPlayer.Death, GameId = requestedPlayer.GameId, IsHuman = requestedPlayer.IsHuman, Kills = requestedPlayer.Kills, PlayerName = requestedPlayer.PlayerName, UserId = requestedPlayer.UserId };
            }

            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            if (currentPlayer == null || !PlayerIsAdminOfGame(currentPlayer, currentGame) && currentPlayer.Id != requestedPlayer.Id)
            {
                return new Player() { Id = requestedPlayer.Id, Death = requestedPlayer.Death, GameId = requestedPlayer.GameId, IsHuman = requestedPlayer.IsHuman, Kills = requestedPlayer.Kills, PlayerName = requestedPlayer.PlayerName, UserId = requestedPlayer.UserId };
            }
            // Return to admin, or player owner.
            return requestedPlayer;
        }

        // GET: api/game/1/Player/me
        // Get the current logged-in users player in a game.
        [HttpGet("me")]
        public async Task<ActionResult<Player>> GetMyPlayer(int gameId)
        {
            User currentUser = await GetLoggedInUser();

            if (currentUser == null)
            {
                return Forbid();
            }

            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            if (currentPlayer == null)
            {
                return NotFound();
            }

            return currentPlayer;
        }

        // PUT: api/game/1/Player/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlayer(int id, Player player, int gameId)
        {

            if (id != player.Id)
            {
                return BadRequest();
            } 
            else if (player.GameId != gameId)
            {
                return BadRequest();
            }

            User currentUser = await GetLoggedInUser();

            if (currentUser == null)
            {
                return Forbid();
            }

            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            if (currentPlayer == null || !currentPlayer.IsGameAdmin || currentPlayer.GameId != gameId)
            {
                return Forbid();
            }

            // Detach currentplayer to start altering the other player object.
            _context.Entry(currentPlayer).State = EntityState.Detached;

            _context.Entry(player).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerExists(id, gameId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/game/1/Player
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Player>> PostPlayer(Player player, int gameId)
        {
            if (player.GameId != gameId)
            {
                return BadRequest();
            }

            User currentUser = await GetLoggedInUser();
            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            // Can't add 2 players to same game, and can only make player for your own user.
            if (currentPlayer != null || player.UserId != currentUser.Id)
            {
                return BadRequest();
            }

            _context.Players.Add(player);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPlayer", new { id = player.Id }, player);
        }

        // DELETE: api/game/1/Player/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Player>> DeletePlayer(int id, int gameId)
        {
            var player = await _context.Players.FindAsync(id);
            if (player == null || player.GameId != gameId)
            {
                return BadRequest();
            }

            User currentUser = await GetLoggedInUser();

            if (currentUser == null)
            {
                return Forbid();
            }

            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);

            if (currentPlayer == null || !currentPlayer.IsGameAdmin || currentPlayer.GameId != gameId)
            {
                return Forbid();
            }

            _context.Players.Remove(player);
            await _context.SaveChangesAsync();

            return player;
        }

        private bool PlayerExists(int id, int gameId)
        {
            return _context.Players.Any(e => e.Id == id && e.GameId == gameId);
        }

        private bool GameExists(int gameId)
        {
            return _context.Games.Any(g => g.Id == gameId);
        }

        private async Task<User> GetLoggedInUser()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            return await _context.Users.FindAsync(userId);
        }

        private async Task<Player> GetLoggedInPlayer(User currentUser, int gameId)
        {
            return await _context.Players.Where(p => p.UserId == currentUser.Id && p.GameId == gameId).FirstOrDefaultAsync();
        }

        private bool PlayerIsAdminOfGame(Player player, Game game)
        {
            return player.IsGameAdmin && player.GameId == game.Id;
        }
    }
}

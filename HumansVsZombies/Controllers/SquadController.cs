﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using HumansVsZombies.Data;
using HumansVsZombies.Models;


namespace HumansVsZombies.Controllers
{
    [Route("api/game/{gameId}/[controller]")]
    [ApiController]
    [Authorize]
    public class SquadController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public SquadController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/game/1/Squad
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<Squad>>> GetSquads(int gameId)
        {
            var game = await _context.Games.FindAsync(gameId);

            if (game == null)
            {
                return NotFound();
            }

            return await _context.Squads.Where(s => s.GameId == gameId).ToListAsync();
        }

        // GET: api/game/1/Squad/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<Squad>> GetSquad(int id, int gameId)
        {
            var squad = await _context.Squads.FindAsync(id);

            if (squad == null || squad.GameId != gameId)
            {
                return NotFound();
            }

            return squad;
        }

        // PUT: api/game/1/Squad/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSquad(int id, Squad squad, int gameId)
        {
            if (id != squad.Id)
            {
                return BadRequest("Error: Mismatched squad id and squad object.");
            } 
            else if (squad.GameId != gameId)
            {
                return NotFound();
            }

            // Find current user and verify that it's not null
            User currentUser = await GetLoggedInUser();
            if (currentUser == null) return Unauthorized();

            // Find current player and verify that it's not null
            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);
            if (currentPlayer == null || !currentPlayer.IsGameAdmin || currentPlayer.GameId != gameId) return Forbid();

            _context.Entry(squad).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SquadExists(id, gameId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/game/1/Squad
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Squad>> PostSquad(Squad squad, int gameId)
        {
            // Find current user and verify that it's not null
            User currentUser = await GetLoggedInUser();
            if (currentUser == null) return Unauthorized();

            // Find current player and verify that it's not null
            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);
            if (currentPlayer == null) return Forbid();

            if (squad.GameId != gameId) return BadRequest("Error: Can't add squad to other game.");
            if (currentPlayer.GameId != gameId) return BadRequest("Error: Player must belong to current game.");

            // Add the new objects to database and save changes
            _context.Squads.Add(squad);
            await _context.SaveChangesAsync();

            // Create squad membership object for the relation between player and squad
            SquadMember sm = new SquadMember() { GameId = gameId, PlayerId = currentPlayer.Id, Rank = Rank.Leader, SquadId = squad.Id };
            _context.SquadMembers.Add(sm);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSquad", new { id = squad.Id }, squad);
        }

        // DELETE: api/game/1/Squad/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Squad>> DeleteSquad(int id, int gameId)
        {
            // Find the referenced squad and verify that it's not null
            var squad = await _context.Squads.Where(s => s.GameId == gameId && s.Id == id).FirstOrDefaultAsync();
            if (squad == null) return NotFound();

            // Find current user and verify that it's not null
            User currentUser = await GetLoggedInUser();
            if (currentUser == null) return Unauthorized();

            // Find current player and verify that it's not null
            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);
            if (currentPlayer == null || !currentPlayer.IsGameAdmin) return Forbid();

            if (currentPlayer.GameId != gameId) return BadRequest("Error: Player must belong to current game.");

            // Remove the object and save changes
            _context.Squads.Remove(squad);
            await _context.SaveChangesAsync();

            return squad;
        }

        //Join a squad
        // POST: api/game/1/Squad/5/join
        [HttpPost("{id}/join")]
        public async Task<ActionResult<SquadMember>> JoinSquad(SquadMember squadMember, int id, int gameId)
        {
            // Find current user and verify that it's not null
            User currentUser = await GetLoggedInUser();
            if (currentUser == null) return Unauthorized();

            Squad squad = await _context.Squads.FindAsync(id);
            if (squadMember.GameId != gameId || squad.GameId != gameId) return NotFound();

            // Find current player and verify that it's not null
            Player currentPlayer = await _context.Players.FindAsync(squadMember.PlayerId);
            if (currentPlayer == null) return Forbid();

            if (currentPlayer.GameId != gameId) return BadRequest("Error: Player must belong to current game.");
            if (squad.IsHuman && !currentPlayer.IsHuman) return BadRequest("Error: You can't join a human squad as a zombie!");

            // Add the relation object and save changes
            _context.SquadMembers.Add(squadMember);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSquadMember", new { id = squadMember.Id }, squadMember);
        }
        // Leave a squad
        // DELETE: api/game/1/Squad/5/leave
        [HttpDelete("{id}/leave")]
        public async Task<ActionResult<SquadMember>> LeaveSquad(int id, int gameId)
        {
            User currentUser = await GetLoggedInUser();

            // Find current player and verify that it's not null
            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);
            if (currentPlayer == null) return Forbid();

            // Find the referenced squadMember and verify that it's not null
            var squadMember = await _context.SquadMembers.Where(sm => sm.GameId == gameId && sm.PlayerId == currentPlayer.Id).FirstOrDefaultAsync();
            if (squadMember == null) return NotFound();

            // Find the squad
            Squad squad = await _context.Squads.FindAsync(id);

            if (squadMember.GameId != gameId)
            {
                return BadRequest("Game does not match!");
            }
            else if (squad.GameId != gameId)
            {
                return BadRequest("Squad does not match game!");
            }

            _context.SquadMembers.Remove(squadMember);
            await _context.SaveChangesAsync();

            return squadMember;
        }

        // SquadCheckin

            // GET: api/game/1/squad/5/check-in
        [HttpGet("{id}/check-in")]
        public async Task<ActionResult<IEnumerable<SquadCheckIn>>> GetSquadCheckIn(int id, int gameId)
        {
            // Find current user and verify that it's not null
            User currentUser = await GetLoggedInUser();
            if (currentUser == null) return Unauthorized();

            Squad squad = await _context.Squads.FindAsync(id);
            if (squad == null || squad.GameId != gameId) return NotFound();

            // Find current player and verify that it's valid
            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);
            if (currentPlayer == null || !squad.HasMember(currentPlayer)) return Forbid();
            if (currentPlayer.GameId != gameId) return BadRequest("Error: Player must belong to current game.");

            return await _context.SquadCheckIns.Where(sc => sc.SquadId == id).ToListAsync();
        }

        // POST: api/game/1/squad/5/check-in
        [HttpPost("{id}/check-in")]
        public async Task<ActionResult<SquadCheckIn>> CreateSquadCheckIn(SquadCheckIn squadCheckIn, int id, int gameId)
        {
            // Find current user and verify that it's not null
            User currentUser = await GetLoggedInUser();
            if (currentUser == null) return Unauthorized();

            Squad squad = await _context.Squads.FindAsync(id);
            if (squad == null || squad.GameId != gameId) return NotFound();

            // Find current player and verify that it's not null
            Player currentPlayer = await GetLoggedInPlayer(currentUser, gameId);
            if (currentPlayer == null || !squad.HasMember(currentPlayer)) return Forbid();
            if (currentPlayer.GameId != gameId) return BadRequest("Error: Player must belong to current game.");

            if (squadCheckIn.SquadId != squad.Id) return BadRequest("Error: Squad check-in must belong to the squad!");

            // Add check-in object and save changes
            _context.SquadCheckIns.Add(squadCheckIn);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSquadCheckIn", new { id = squadCheckIn.Id }, squadCheckIn);
        }

        private bool SquadExists(int id, int gameId) => _context.Squads.Any(e => e.Id == id && e.GameId == gameId);

        private async Task<User> GetLoggedInUser() =>  await _context.Users.FindAsync(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);

        private async Task<Player> GetLoggedInPlayer(User currentUser, int gameId) =>  await _context.Players.Where(p => p.UserId == currentUser.Id && p.GameId == gameId).FirstOrDefaultAsync();
    }
}

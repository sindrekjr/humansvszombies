﻿using HumansVsZombies.Models;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HumansVsZombies.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<User>
    {
        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
            this.Database.Migrate();
        }

        public DbSet<Game> Games { get; set; }
        public DbSet<Kill> Kills { get; set; }
        public DbSet<Mission> Missions { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Squad> Squads { get; set; }
        public DbSet<SquadCheckIn> SquadCheckIns { get; set; }
        public DbSet<SquadMember> SquadMembers { get; set; }
        public override DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseLazyLoadingProxies();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Foreign key for the killer
            modelBuilder.Entity<Kill>()
                .HasOne(kill => kill.Killer)
                .WithMany(killer => killer.Kills)
                .HasForeignKey(k => k.KillerId)
                .OnDelete(DeleteBehavior.Restrict);

            // Foreign key for the victim
            modelBuilder.Entity<Kill>()
                .HasOne(kill => kill.Victim)
                .WithOne(v => v.Death)
                .HasForeignKey<Kill>(k => k.VictimId)
                .OnDelete(DeleteBehavior.Restrict);

            // Remove cascading delete on Squadmembers
            modelBuilder.Entity<SquadMember>()
                .HasOne(sm => sm.Player)
                .WithMany(p => p.SquadMembers)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<SquadMember>()
                .HasOne(sm => sm.Squad)
                .WithMany(s => s.SquadMembers)
                .OnDelete(DeleteBehavior.Restrict);

            // Remove cascading delete on SquadCheckin
            modelBuilder.Entity<SquadCheckIn>()
                .HasOne(sc => sc.Squad)
                .WithMany(s => s.SquadCheckIns)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<SquadCheckIn>()
                .HasOne(sc => sc.SquadMember)
                .WithMany(s => s.SquadCheckIns)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<Squad>()
                .HasMany(s => s.SquadMembers)
                .WithOne(sm => sm.Squad)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Player>()
                .HasMany(p => p.Kills)
                .WithOne(k => k.Killer)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Player>()
                .HasOne(p => p.Death)
                .WithOne(k => k.Victim)
                .OnDelete(DeleteBehavior.Cascade);


            /// Seed data
            ApplicationDbSeeder.Seed(modelBuilder);
        }   
    }
}

using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;

using HumansVsZombies.Models;

namespace HumansVsZombies.Data
{
    public static class ApplicationDbSeeder
    {
        static HashSet<Game> Games = new HashSet<Game>();
        static HashSet<Player> Players = new HashSet<Player>();
        static HashSet<Squad> Squads = new HashSet<Squad>();
        static HashSet<SquadMember> SquadMembers = new HashSet<SquadMember>();
        static HashSet<Kill> Kills = new HashSet<Kill>();

        public static void Seed(ModelBuilder builder)
        {
            builder.Entity<User>().HasData(
                new User { Id = "0001", UserName = "SeedUser1" },
                new User { Id = "0002", UserName = "SeedUser2" },
                new User { Id = "0003", UserName = "SeedUser3" },
                new User { Id = "0004", UserName = "SeedUser4" },
                new User { Id = "0005", UserName = "SeedUser5" },
                new User { Id = "0006", UserName = "SeedUser6" }
            );

            DeadSnow();
            WalkingDead();
            TwentyEightDaysLater();
            ResidentEvil();
            EvilDead();
            PlanetTerror();

            builder.Entity<Game>().HasData(Games);
            builder.Entity<Player>().HasData(Players);
            builder.Entity<Squad>().HasData(Squads);
            builder.Entity<SquadMember>().HasData(SquadMembers);
            builder.Entity<Kill>().HasData(Kills);
        }

        static void DeadSnow()
        {
            var game = new Game
            {
                Id = 1000,
                Name = "Dead Snow",
                GameState = GameState.Complete,
                StartTime = DateTime.Now.AddYears(-1),
                EndTime = DateTime.Now.AddYears(-1).AddHours(2),
                NELat = 60.399819,
                NELng = 5.355332,
                SWLat = 60.380307,
                SWLng = 5.295871
            };
            var players = new List<Player> {
                new Player { Id = 10001, UserId = "0001", GameId = 1000, PlayerName = "Martin", IsHuman = true, IsPatientZero = false, IsGameAdmin = true, BiteCode = "SeedCode0001" },
                new Player { Id = 10002, UserId = "0002", GameId = 1000, PlayerName = "Herzog", IsHuman = true, IsPatientZero = true, IsGameAdmin = false, BiteCode = "SeedCode0002" },
                new Player { Id = 10003, UserId = "0003", GameId = 1000, PlayerName = "Hanna", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0003" },
                new Player { Id = 10004, UserId = "0004", GameId = 1000, PlayerName = "Vegard", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0004" },
                new Player { Id = 10005, UserId = "0005", GameId = 1000, PlayerName = "Liv", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0005" },
                new Player { Id = 10006, UserId = "0006", GameId = 1000, PlayerName = "Roy", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0006" }
            };
            var squads = new List<Squad>
            {
                new Squad { Id = 10001, GameId = game.Id, Name = "Travelers" }
            };
            var memberships = new List<SquadMember>
            {
                new SquadMember { Id = 1000110001, SquadId = 10001, PlayerId = 10001, GameId = game.Id },
                new SquadMember { Id = 1000110002, SquadId = 10001, PlayerId = 10002, GameId = game.Id },
                new SquadMember { Id = 1000110003, SquadId = 10001, PlayerId = 10003, GameId = game.Id },
                new SquadMember { Id = 1000110004, SquadId = 10001, PlayerId = 10004, GameId = game.Id },
                new SquadMember { Id = 1000110005, SquadId = 10001, PlayerId = 10005, GameId = game.Id },
                new SquadMember { Id = 1000110006, SquadId = 10001, PlayerId = 10006, GameId = game.Id }
            };

            var kills = GenerateKills(game, players);

            Games.Add(game);
            Players.UnionWith(players);
            Squads.UnionWith(squads);
            SquadMembers.UnionWith(memberships);
            Kills.UnionWith(kills);
        }

        static void WalkingDead()
        {
            var game = new Game
            {
                Id = 1001,
                Name = "The Walking Dead",
                GameState = GameState.Complete,
                StartTime = DateTime.Now.AddMonths(-10),
                EndTime = DateTime.Now.AddMonths(-10).AddHours(4),
                NELat = 60.399819,
                NELng = 5.355332,
                SWLat = 60.380307,
                SWLng = 5.295871
            };
            var players = new List<Player>
            {
                new Player { Id = 10011, UserId = "0001", GameId = 1001, PlayerName = "Rick", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0001" },
                new Player { Id = 10012, UserId = "0002", GameId = 1001, PlayerName = "Daryl", IsHuman = true, IsPatientZero = false, IsGameAdmin = true, BiteCode = "SeedCode0002" },
                new Player { Id = 10013, UserId = "0003", GameId = 1001, PlayerName = "Carol", IsHuman = true, IsPatientZero = true, IsGameAdmin = false, BiteCode = "SeedCode0003" },
                new Player { Id = 10014, UserId = "0004", GameId = 1001, PlayerName = "Morgan", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0004" },
                new Player { Id = 10015, UserId = "0005", GameId = 1001, PlayerName = "Maggie", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0005" },
                new Player { Id = 10016, UserId = "0006", GameId = 1001, PlayerName = "Gabriel", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0006" }
            };
            var squads = new List<Squad>
            {
                new Squad { Id = 10011, GameId = game.Id, Name = "Prisoners" },
                new Squad { Id = 10012, GameId = game.Id, Name = "Marauders" },
                new Squad { Id = 10013, GameId = game.Id, Name = "Hunters" }
            };

            var memberships = new List<SquadMember>
            {
                new SquadMember { Id = 1001110011, SquadId = 10011, PlayerId = 10011, GameId = game.Id },
                new SquadMember { Id = 1001110012, SquadId = 10011, PlayerId = 10012, GameId = game.Id },
                new SquadMember { Id = 1001210013, SquadId = 10012, PlayerId = 10013, GameId = game.Id },
                new SquadMember { Id = 1001210014, SquadId = 10012, PlayerId = 10014, GameId = game.Id },
                new SquadMember { Id = 1001310015, SquadId = 10013, PlayerId = 10015, GameId = game.Id },
                new SquadMember { Id = 1001310016, SquadId = 10013, PlayerId = 10016, GameId = game.Id }
            };

            var kills = GenerateKills(game, players);

            Games.Add(game);
            Players.UnionWith(players);
            Squads.UnionWith(squads);
            SquadMembers.UnionWith(memberships);
            Kills.UnionWith(kills);
        }

        static void TwentyEightDaysLater()
        {
            var game = new Game
            {
                Id = 1002,
                Name = "28 Days Later",
                GameState = GameState.Complete,
                StartTime = DateTime.Now.AddMonths(-10).AddDays(8),
                EndTime = DateTime.Now.AddMonths(-10).AddDays(8).AddDays(1),
                NELat = 60.399819,
                NELng = 5.355332,
                SWLat = 60.380307,
                SWLng = 5.295871
            };
            var players = new List<Player>
            {
                new Player { Id = 10021, UserId = "0001", GameId = 1002, PlayerName = "Jim", IsHuman = true, IsPatientZero = true, IsGameAdmin = false, BiteCode = "SeedCode0001" },
                new Player { Id = 10022, UserId = "0002", GameId = 1002, PlayerName = "Selena", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0002" },
                new Player { Id = 10023, UserId = "0003", GameId = 1002, PlayerName = "Frank", IsHuman = true, IsPatientZero = false, IsGameAdmin = true, BiteCode = "SeedCode0003" },
                new Player { Id = 10024, UserId = "0004", GameId = 1002, PlayerName = "Hannah", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0004" },
                new Player { Id = 10025, UserId = "0005", GameId = 1002, PlayerName = "Henry West", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0005" },
                new Player { Id = 10026, UserId = "0006", GameId = 1002, PlayerName = "Private Clifton", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0006" }
            };
            var squads = new List<Squad>
            {
                new Squad { Id = 10021, GameId = game.Id, Name = "Loners" },
                new Squad { Id = 10022, GameId = game.Id, Name = "Shoppers" },
                new Squad { Id = 10023, GameId = game.Id, Name = "Army" }
            };
            var memberships = new List<SquadMember>
            {
                new SquadMember { Id = 1002110021, SquadId = 10021, PlayerId = 10021, GameId = game.Id },
                new SquadMember { Id = 1002110022, SquadId = 10021, PlayerId = 10022, GameId = game.Id },
                new SquadMember { Id = 1002210023, SquadId = 10022, PlayerId = 10023, GameId = game.Id },
                new SquadMember { Id = 1002210024, SquadId = 10022, PlayerId = 10024, GameId = game.Id },
                new SquadMember { Id = 1002310025, SquadId = 10023, PlayerId = 10025, GameId = game.Id },
                new SquadMember { Id = 1002310026, SquadId = 10023, PlayerId = 10026, GameId = game.Id }
            };

            var kills = GenerateKills(game, players);

            Games.Add(game);
            Players.UnionWith(players);
            Squads.UnionWith(squads);
            SquadMembers.UnionWith(memberships);
            Kills.UnionWith(kills);
        }

        static void ResidentEvil()
        {
            var game = new Game
            {
                Id = 1003,
                Name = "Resident Evil",
                GameState = GameState.Complete,
                StartTime = DateTime.Now.AddMonths(-5),
                EndTime = DateTime.Now.AddMonths(-5).AddHours(12),
                NELat = 60.399819,
                NELng = 5.355332,
                SWLat = 60.380307,
                SWLng = 5.295871
            };
            var players = new List<Player>
            {
                new Player { Id = 10031, UserId = "0001", GameId = 1003, PlayerName = "Alice", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0001" },
                new Player { Id = 10032, UserId = "0002", GameId = 1003, PlayerName = "Nemesis", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0002" },
                new Player { Id = 10033, UserId = "0003", GameId = 1003, PlayerName = "Tyrant", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0003" },
                new Player { Id = 10034, UserId = "0004", GameId = 1003, PlayerName = "Red Queen", IsHuman = true, IsPatientZero = false, IsGameAdmin = true, BiteCode = "SeedCode0004" },
                new Player { Id = 10035, UserId = "0005", GameId = 1003, PlayerName = "T", IsHuman = true, IsPatientZero = true, IsGameAdmin = false, BiteCode = "SeedCode0005" },
                new Player { Id = 10036, UserId = "0006", GameId = 1003, PlayerName = "Valentine", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0006" }
            };
            var squads = new List<Squad>
            {
                new Squad { Id = 10031, GameId = game.Id, Name = "Commandos" },
                new Squad { Id = 10032, GameId = game.Id, Name = "Scientists" },
                new Squad { Id = 10033, GameId = game.Id, Name = "AI" }
            };
            var memberships = new List<SquadMember>
            {
                new SquadMember { Id = 1003110031, SquadId = 10031, PlayerId = 10031, GameId = game.Id },
                new SquadMember { Id = 1003110036, SquadId = 10031, PlayerId = 10036, GameId = game.Id },
                new SquadMember { Id = 1003210033, SquadId = 10032, PlayerId = 10033, GameId = game.Id },
                new SquadMember { Id = 1003210032, SquadId = 10032, PlayerId = 10032, GameId = game.Id },
                new SquadMember { Id = 1003310035, SquadId = 10033, PlayerId = 10035, GameId = game.Id },
                new SquadMember { Id = 1003310034, SquadId = 10033, PlayerId = 10034, GameId = game.Id }
            };

            var kills = GenerateKills(game, players);

            Games.Add(game);
            Players.UnionWith(players);
            Squads.UnionWith(squads);
            SquadMembers.UnionWith(memberships);
            Kills.UnionWith(kills);
        }

        static void EvilDead()
        {
            var game = new Game
            {
                Id = 1004,
                Name = "Evil Dead",
                GameState = GameState.Registration,
                StartTime = DateTime.Now.AddMonths(1),
                EndTime = DateTime.Now.AddMonths(1).AddMonths(1),
                NELat = 60.399819,
                NELng = 5.355332,
                SWLat = 60.380307,
                SWLng = 5.295871
            };
            var players = new List<Player>
            {
                new Player { Id = 10041, UserId = "0001", GameId = 1004, PlayerName = "Ash", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0001" },
                new Player { Id = 10042, UserId = "0002", GameId = 1004, PlayerName = "Linda", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0002" },
                new Player { Id = 10043, UserId = "0003", GameId = 1004, PlayerName = "Scotty", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0003" },
                new Player { Id = 10044, UserId = "0004", GameId = 1004, PlayerName = "Henry the Red", IsHuman = true, IsPatientZero = true, IsGameAdmin = true, BiteCode = "SeedCode0004" }
            };
            var squads = new List<Squad>
            {
                new Squad { Id = 10041, GameId = game.Id, Name = "Ash" },
                new Squad { Id = 10042, GameId = game.Id, Name = "Ancient Evil" }
            };
            var memberships = new List<SquadMember>
            {
                new SquadMember { Id = 1004110041, SquadId = 10041, PlayerId = 10041, GameId = game.Id },
                new SquadMember { Id = 1004110042, SquadId = 10041, PlayerId = 10042, GameId = game.Id },
                new SquadMember { Id = 1004210043, SquadId = 10042, PlayerId = 10043, GameId = game.Id },
                new SquadMember { Id = 1004210044, SquadId = 10042, PlayerId = 10044, GameId = game.Id }
            };

            Games.Add(game);
            Players.UnionWith(players);
            Squads.UnionWith(squads);
            SquadMembers.UnionWith(memberships);
        }

        static void PlanetTerror()
        {
            var game = new Game
            {
                Id = 1005,
                Name = "Planet Terror",
                GameState = GameState.Registration,
                StartTime = DateTime.Now.AddMonths(3),
                EndTime = DateTime.Now.AddMonths(3).AddHours(6),
                NELat = 60.399819,
                NELng = 5.355332,
                SWLat = 60.380307,
                SWLng = 5.295871
            };
            var players = new List<Player>
            {
                new Player { Id = 10051, UserId = "0001", GameId = 1005, PlayerName = "Cherry", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0001" },
                new Player { Id = 10052, UserId = "0002", GameId = 1005, PlayerName = "Muldoon", IsHuman = true, IsPatientZero = false, IsGameAdmin = true, BiteCode = "SeedCode0002" },
                new Player { Id = 10053, UserId = "0003", GameId = 1005, PlayerName = "El Wray", IsHuman = true, IsPatientZero = true, IsGameAdmin = false, BiteCode = "SeedCode0003" },
                new Player { Id = 10054, UserId = "0004", GameId = 1005, PlayerName = "Dakota", IsHuman = true, IsPatientZero = false, IsGameAdmin = false, BiteCode = "SeedCode0004" }
            };
            var squads = new List<Squad>
            {
                new Squad { Id = 10051, GameId = game.Id, Name = "Military" },
                new Squad { Id = 10052, GameId = game.Id, Name = "Ladies" }
            };
            var memberships = new List<SquadMember>
            {
                new SquadMember { Id = 1005110052, SquadId = 10051, PlayerId = 10052, GameId = game.Id },
                new SquadMember { Id = 1005110053, SquadId = 10051, PlayerId = 10053, GameId = game.Id },
                new SquadMember { Id = 1005210051, SquadId = 10052, PlayerId = 10051, GameId = game.Id },
                new SquadMember { Id = 1005210054, SquadId = 10052, PlayerId = 10054, GameId = game.Id }
            };

            Games.Add(game);
            Players.UnionWith(players);
            Squads.UnionWith(squads);
            SquadMembers.UnionWith(memberships);
        }

        static IEnumerable<Kill> GenerateKills(Game game, IEnumerable<Player> players)
        {
            var random = new Random();
            players.First(p => p.IsPatientZero).IsHuman = false;

            foreach(var victim in players.Where(p => p.IsHuman))
            {
                var zombies = players.Where(p => !p.IsHuman);
                var killer = zombies.ElementAt(random.Next(0, zombies.Count()));

                victim.IsHuman = false;
                yield return new Kill
                {
                    Id = (victim.Id * 100000) + killer.Id,
                    KillerId = killer.Id,
                    VictimId = victim.Id,
                    GameId = game.Id,
                    BiteCode = victim.BiteCode,
                    Story = "Zombie bit human. The end.",

                    TimeOfDeath = DateTime.Now,
                    Lat = random.NextDouble() * (game.NELat - game.SWLat) + game.SWLat,
                    Lng = random.NextDouble() * (game.NELng - game.SWLng) + game.SWLng
                };
            }
        }
    }
}
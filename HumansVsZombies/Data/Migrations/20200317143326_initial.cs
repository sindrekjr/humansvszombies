﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Games",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    GameState = table.Column<int>(nullable: false),
                    NELat = table.Column<double>(nullable: true),
                    NELng = table.Column<double>(nullable: true),
                    SWLat = table.Column<double>(nullable: true),
                    SWLng = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Games", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Missions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GameId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsHumanVisible = table.Column<bool>(nullable: false),
                    IsZombieVisible = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    StartTime = table.Column<DateTime>(nullable: true),
                    EndTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Missions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Missions_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Players",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: true),
                    GameId = table.Column<int>(nullable: false),
                    BiteCode = table.Column<string>(nullable: true),
                    IsHuman = table.Column<bool>(nullable: false),
                    IsPatientZero = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Players", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Players_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Players_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Squads",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GameId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsHuman = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Squads", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Squads_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Kills",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    KillerId = table.Column<int>(nullable: false),
                    VictimId = table.Column<int>(nullable: false),
                    GameId = table.Column<int>(nullable: false),
                    TimeOfDeath = table.Column<DateTime>(nullable: false),
                    Story = table.Column<string>(nullable: true),
                    Lat = table.Column<double>(nullable: true),
                    Lng = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kills", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Kills_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Kills_Players_KillerId",
                        column: x => x.KillerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Kills_Players_VictimId",
                        column: x => x.VictimId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SquadMembers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PlayerId = table.Column<int>(nullable: false),
                    SquadId = table.Column<int>(nullable: false),
                    GameId = table.Column<int>(nullable: false),
                    Rank = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SquadMembers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SquadMembers_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SquadMembers_Players_PlayerId",
                        column: x => x.PlayerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SquadMembers_Squads_SquadId",
                        column: x => x.SquadId,
                        principalTable: "Squads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SquadCheckIns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SquadMemberId = table.Column<int>(nullable: false),
                    SquadId = table.Column<int>(nullable: false),
                    GameId = table.Column<int>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false),
                    Lat = table.Column<double>(nullable: false),
                    Lng = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SquadCheckIns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SquadCheckIns_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SquadCheckIns_Squads_SquadId",
                        column: x => x.SquadId,
                        principalTable: "Squads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SquadCheckIns_SquadMembers_SquadMemberId",
                        column: x => x.SquadMemberId,
                        principalTable: "SquadMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "1", 0, "95379368-6501-47e3-abf3-753012a3e8c8", null, false, "Player", "One", false, null, null, null, null, null, false, "a966a77a-c3bc-45eb-aae6-e07c4276f1ac", false, null });

            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "Id", "GameState", "NELat", "NELng", "Name", "SWLat", "SWLng" },
                values: new object[] { 420, 1, null, null, "Testing the Game", null, null });

            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "Id", "GameState", "NELat", "NELng", "Name", "SWLat", "SWLng" },
                values: new object[] { 69, 0, null, null, "TestGame", null, null });

            migrationBuilder.InsertData(
                table: "Missions",
                columns: new[] { "Id", "Description", "EndTime", "GameId", "IsHumanVisible", "IsZombieVisible", "Name", "StartTime" },
                values: new object[,]
                {
                    { 1, "Do a mission", new DateTime(2020, 3, 22, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(6017), 69, true, true, null, new DateTime(2020, 3, 17, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(5593) },
                    { 2, "Do a mission1", new DateTime(2020, 4, 11, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(7028), 69, false, true, null, new DateTime(2020, 3, 17, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(7014) },
                    { 3, "Do a mission2", new DateTime(2020, 3, 22, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(7058), 420, true, false, null, new DateTime(2020, 3, 17, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(7054) }
                });

            migrationBuilder.InsertData(
                table: "Players",
                columns: new[] { "Id", "BiteCode", "GameId", "IsHuman", "IsPatientZero", "UserId" },
                values: new object[,]
                {
                    { 1, "BiteCode1", 69, true, false, "1" },
                    { 2, "BiteCode12", 69, false, true, "1" },
                    { 3, "BiteCode1233", 420, false, true, "1" },
                    { 4, "BiteCode12334", 420, true, false, "1" }
                });

            migrationBuilder.InsertData(
                table: "Squads",
                columns: new[] { "Id", "GameId", "IsHuman", "Name" },
                values: new object[,]
                {
                    { 1, 69, true, "Bookhouse Boys" },
                    { 2, 69, false, "Bookhouse Girls" },
                    { 3, 420, false, "Bankers" },
                    { 4, 420, true, "Communists" }
                });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[] { 1, 420, 3, null, null, "It happened", new DateTime(2020, 3, 17, 15, 33, 26, 67, DateTimeKind.Local).AddTicks(3705), 4 });

            migrationBuilder.InsertData(
                table: "SquadMembers",
                columns: new[] { "Id", "GameId", "PlayerId", "Rank", "SquadId" },
                values: new object[,]
                {
                    { 1, 69, 1, 0, 1 },
                    { 2, 69, 2, 0, 2 },
                    { 3, 420, 3, 0, 3 },
                    { 4, 420, 4, 0, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Kills_GameId",
                table: "Kills",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_Kills_KillerId",
                table: "Kills",
                column: "KillerId");

            migrationBuilder.CreateIndex(
                name: "IX_Kills_VictimId",
                table: "Kills",
                column: "VictimId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Missions_GameId",
                table: "Missions",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_Players_GameId",
                table: "Players",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_Players_UserId",
                table: "Players",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SquadCheckIns_GameId",
                table: "SquadCheckIns",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_SquadCheckIns_SquadId",
                table: "SquadCheckIns",
                column: "SquadId");

            migrationBuilder.CreateIndex(
                name: "IX_SquadCheckIns_SquadMemberId",
                table: "SquadCheckIns",
                column: "SquadMemberId");

            migrationBuilder.CreateIndex(
                name: "IX_SquadMembers_GameId",
                table: "SquadMembers",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_SquadMembers_PlayerId",
                table: "SquadMembers",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_SquadMembers_SquadId",
                table: "SquadMembers",
                column: "SquadId");

            migrationBuilder.CreateIndex(
                name: "IX_Squads_GameId",
                table: "Squads",
                column: "GameId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Kills");

            migrationBuilder.DropTable(
                name: "Missions");

            migrationBuilder.DropTable(
                name: "SquadCheckIns");

            migrationBuilder.DropTable(
                name: "SquadMembers");

            migrationBuilder.DropTable(
                name: "Players");

            migrationBuilder.DropTable(
                name: "Squads");

            migrationBuilder.DropTable(
                name: "Games");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "AspNetUsers");
        }
    }
}

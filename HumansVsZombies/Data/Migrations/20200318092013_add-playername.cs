﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class addplayername : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PlayerName",
                table: "Players",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "82eb3c33-7857-4205-a524-72fdf20551f2", "ff2d30da-2202-4869-bd46-5d18d23639c1" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1,
                column: "TimeOfDeath",
                value: new DateTime(2020, 3, 18, 10, 20, 12, 778, DateTimeKind.Local).AddTicks(9447));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 23, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(2526), new DateTime(2020, 3, 18, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(2111) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 4, 12, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(3480), new DateTime(2020, 3, 18, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(3467) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 23, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(3504), new DateTime(2020, 3, 18, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(3501) });

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 1,
                column: "PlayerName",
                value: "twitchyphoenix");

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 2,
                column: "PlayerName",
                value: "izombie");

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 3,
                column: "PlayerName",
                value: "rastafarianan");

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 4,
                column: "PlayerName",
                value: "killerman");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlayerName",
                table: "Players");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "95379368-6501-47e3-abf3-753012a3e8c8", "a966a77a-c3bc-45eb-aae6-e07c4276f1ac" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1,
                column: "TimeOfDeath",
                value: new DateTime(2020, 3, 17, 15, 33, 26, 67, DateTimeKind.Local).AddTicks(3705));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 22, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(6017), new DateTime(2020, 3, 17, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(5593) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 4, 11, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(7028), new DateTime(2020, 3, 17, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(7014) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 22, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(7058), new DateTime(2020, 3, 17, 15, 33, 26, 69, DateTimeKind.Local).AddTicks(7054) });
        }
    }
}

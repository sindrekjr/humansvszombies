﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class addisgameadminproperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsGameAdmin",
                table: "Players",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "70e891a8-aa73-46d5-b042-f7f812b36b3d", "47253e4b-5f12-4ca9-bc1c-73d0755d24d1" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1,
                column: "TimeOfDeath",
                value: new DateTime(2020, 3, 24, 11, 45, 35, 292, DateTimeKind.Local).AddTicks(1014));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 29, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(3808), new DateTime(2020, 3, 24, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(3393) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 4, 18, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(4759), new DateTime(2020, 3, 24, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(4746) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 29, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(4783), new DateTime(2020, 3, 24, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(4780) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsGameAdmin",
                table: "Players");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "82eb3c33-7857-4205-a524-72fdf20551f2", "ff2d30da-2202-4869-bd46-5d18d23639c1" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1,
                column: "TimeOfDeath",
                value: new DateTime(2020, 3, 18, 10, 20, 12, 778, DateTimeKind.Local).AddTicks(9447));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 23, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(2526), new DateTime(2020, 3, 18, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(2111) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 4, 12, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(3480), new DateTime(2020, 3, 18, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(3467) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 23, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(3504), new DateTime(2020, 3, 18, 10, 20, 12, 781, DateTimeKind.Local).AddTicks(3501) });
        }
    }
}

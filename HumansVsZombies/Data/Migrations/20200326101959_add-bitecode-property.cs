﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class addbitecodeproperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BiteCode",
                table: "Kills",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "23a6b115-c5e5-4e34-bb0b-3d19e151e2c4", "4452b543-9005-47e6-9de2-9c7e7edc73dd" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1,
                column: "TimeOfDeath",
                value: new DateTime(2020, 3, 26, 11, 19, 58, 779, DateTimeKind.Local).AddTicks(1067));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 31, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(4582), new DateTime(2020, 3, 26, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(4141) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 4, 20, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(5590), new DateTime(2020, 3, 26, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(5577) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 31, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(5613), new DateTime(2020, 3, 26, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(5610) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BiteCode",
                table: "Kills");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "70e891a8-aa73-46d5-b042-f7f812b36b3d", "47253e4b-5f12-4ca9-bc1c-73d0755d24d1" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1,
                column: "TimeOfDeath",
                value: new DateTime(2020, 3, 24, 11, 45, 35, 292, DateTimeKind.Local).AddTicks(1014));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 29, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(3808), new DateTime(2020, 3, 24, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(3393) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 4, 18, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(4759), new DateTime(2020, 3, 24, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(4746) });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 3, 29, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(4783), new DateTime(2020, 3, 24, 11, 45, 35, 294, DateTimeKind.Local).AddTicks(4780) });
        }
    }
}

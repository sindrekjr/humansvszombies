﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class addnewseeddata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Kills_Players_KillerId",
                table: "Kills");

            migrationBuilder.DropForeignKey(
                name: "FK_Kills_Players_VictimId",
                table: "Kills");

            migrationBuilder.DropForeignKey(
                name: "FK_SquadMembers_Squads_SquadId",
                table: "SquadMembers");

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Missions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1");

            migrationBuilder.DeleteData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 420);

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "0001", 0, "56007540-859f-4bc0-b13a-e54d4652b5ca", null, false, null, null, false, null, null, null, null, null, false, "aaa3fb52-8858-47cd-985c-3820d6dab0b8", false, "SeedUser1" },
                    { "0002", 0, "5b0a0bbd-e6ac-4b40-94ec-76f23d2a7a20", null, false, null, null, false, null, null, null, null, null, false, "ac5611be-88e1-4294-8eed-1cdf3a31a86e", false, "SeedUser2" },
                    { "0003", 0, "e81a2792-3d76-442d-a6f4-49fe24ebefd8", null, false, null, null, false, null, null, null, null, null, false, "63b5ec61-b789-4d50-8234-e0c0849d39e7", false, "SeedUser3" },
                    { "0004", 0, "4da0a373-f18a-447b-909c-aaec903c86aa", null, false, null, null, false, null, null, null, null, null, false, "7902e141-e4b7-40ac-8033-9f866f6d0041", false, "SeedUser4" },
                    { "0005", 0, "1511c765-22de-4541-9ed4-bd32ece407f7", null, false, null, null, false, null, null, null, null, null, false, "3a687e50-404d-4cdb-8977-c5cde7160a47", false, "SeedUser5" },
                    { "0006", 0, "12620cc9-cc19-4688-82ed-d1ed2c2870e7", null, false, null, null, false, null, null, null, null, null, false, "b01e574a-0e28-4076-b5de-c98b8c7148a0", false, "SeedUser6" }
                });

            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "Id", "GameState", "NELat", "NELng", "Name", "SWLat", "SWLng" },
                values: new object[] { 1000, 2, 60.399819000000001, 5.3553319999999998, "Dead Snow", 60.380307000000002, 5.295871 });

            migrationBuilder.InsertData(
                table: "Players",
                columns: new[] { "Id", "BiteCode", "GameId", "IsGameAdmin", "IsHuman", "IsPatientZero", "PlayerName", "UserId" },
                values: new object[,]
                {
                    { 10001, "SeedCode0001", 1000, true, false, false, "Martin", "0001" },
                    { 10002, "SeedCode0002", 1000, false, false, true, "Herzog", "0002" },
                    { 10003, "SeedCode0003", 1000, false, false, false, "Hanna", "0003" },
                    { 10004, "SeedCode0004", 1000, false, false, false, "Vegard", "0004" },
                    { 10005, "SeedCode0005", 1000, false, false, false, "Liv", "0005" },
                    { 10006, "SeedCode0006", 1000, false, false, false, "Roy", "0006" }
                });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1000110002, "SeedCode0001", 1000, 10002, 60.399277777128923, 60.398050455087329, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 19, 52, 662, DateTimeKind.Local).AddTicks(8927), 10001 },
                    { 1000310001, "SeedCode0003", 1000, 10001, 60.385840094582214, 60.397654787702017, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 19, 52, 665, DateTimeKind.Local).AddTicks(2430), 10003 },
                    { 1000410001, "SeedCode0004", 1000, 10001, 60.386787321725272, 60.399697614628373, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 19, 52, 665, DateTimeKind.Local).AddTicks(2626), 10004 },
                    { 1000510001, "SeedCode0005", 1000, 10001, 60.396960554132548, 60.386173079251535, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 19, 52, 665, DateTimeKind.Local).AddTicks(2645), 10005 },
                    { 1000610004, "SeedCode0006", 1000, 10004, 60.396180489469913, 60.399025863615229, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 19, 52, 665, DateTimeKind.Local).AddTicks(2661), 10006 }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Kills_Players_KillerId",
                table: "Kills",
                column: "KillerId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Kills_Players_VictimId",
                table: "Kills",
                column: "VictimId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_SquadMembers_Squads_SquadId",
                table: "SquadMembers",
                column: "SquadId",
                principalTable: "Squads",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Kills_Players_KillerId",
                table: "Kills");

            migrationBuilder.DropForeignKey(
                name: "FK_Kills_Players_VictimId",
                table: "Kills");

            migrationBuilder.DropForeignKey(
                name: "FK_SquadMembers_Squads_SquadId",
                table: "SquadMembers");

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610004);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10001);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10002);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10003);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10004);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10005);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10006);

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006");

            migrationBuilder.DeleteData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1000);

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "1", 0, "23a6b115-c5e5-4e34-bb0b-3d19e151e2c4", null, false, "Player", "One", false, null, null, null, null, null, false, "4452b543-9005-47e6-9de2-9c7e7edc73dd", false, null });

            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "Id", "GameState", "NELat", "NELng", "Name", "SWLat", "SWLng" },
                values: new object[] { 420, 1, null, null, "Testing the Game", null, null });

            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "Id", "GameState", "NELat", "NELng", "Name", "SWLat", "SWLng" },
                values: new object[] { 69, 0, null, null, "TestGame", null, null });

            migrationBuilder.InsertData(
                table: "Missions",
                columns: new[] { "Id", "Description", "EndTime", "GameId", "IsHumanVisible", "IsZombieVisible", "Name", "StartTime" },
                values: new object[,]
                {
                    { 1, "Do a mission", new DateTime(2020, 3, 31, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(4582), 69, true, true, null, new DateTime(2020, 3, 26, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(4141) },
                    { 2, "Do a mission1", new DateTime(2020, 4, 20, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(5590), 69, false, true, null, new DateTime(2020, 3, 26, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(5577) },
                    { 3, "Do a mission2", new DateTime(2020, 3, 31, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(5613), 420, true, false, null, new DateTime(2020, 3, 26, 11, 19, 58, 781, DateTimeKind.Local).AddTicks(5610) }
                });

            migrationBuilder.InsertData(
                table: "Players",
                columns: new[] { "Id", "BiteCode", "GameId", "IsGameAdmin", "IsHuman", "IsPatientZero", "PlayerName", "UserId" },
                values: new object[,]
                {
                    { 1, "BiteCode1", 69, false, true, false, "twitchyphoenix", "1" },
                    { 2, "BiteCode12", 69, false, false, true, "izombie", "1" },
                    { 3, "BiteCode1233", 420, false, false, true, "rastafarianan", "1" },
                    { 4, "BiteCode12334", 420, false, true, false, "killerman", "1" }
                });

            migrationBuilder.InsertData(
                table: "Squads",
                columns: new[] { "Id", "GameId", "IsHuman", "Name" },
                values: new object[,]
                {
                    { 1, 69, true, "Bookhouse Boys" },
                    { 2, 69, false, "Bookhouse Girls" },
                    { 3, 420, false, "Bankers" },
                    { 4, 420, true, "Communists" }
                });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[] { 1, null, 420, 3, null, null, "It happened", new DateTime(2020, 3, 26, 11, 19, 58, 779, DateTimeKind.Local).AddTicks(1067), 4 });

            migrationBuilder.InsertData(
                table: "SquadMembers",
                columns: new[] { "Id", "GameId", "PlayerId", "Rank", "SquadId" },
                values: new object[,]
                {
                    { 1, 69, 1, 0, 1 },
                    { 2, 69, 2, 0, 2 },
                    { 3, 420, 3, 0, 3 },
                    { 4, 420, 4, 0, 4 }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Kills_Players_KillerId",
                table: "Kills",
                column: "KillerId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Kills_Players_VictimId",
                table: "Kills",
                column: "VictimId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SquadMembers_Squads_SquadId",
                table: "SquadMembers",
                column: "SquadId",
                principalTable: "Squads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

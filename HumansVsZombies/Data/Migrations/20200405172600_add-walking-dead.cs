﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class addwalkingdead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610004);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "530594c5-8352-4f5e-a395-396a613a222b", "d7e42041-ef19-476b-bbda-64def65208ec" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "db382868-6ae7-4f91-a2c7-85d71e5998f1", "e38efcce-9817-4dd1-b6aa-abb46224facc" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "ed763ecf-6465-4813-b7f6-ca6ed0567785", "4e210712-39ec-41ea-8dc0-16b7a7ebd8c1" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "22cdbdba-47aa-42b5-a33a-f14c6f2e2048", "c33f6303-e28d-4b2c-beac-cf662f7032e7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a9b74724-08ed-4a04-9302-7b7d55aba1d6", "28a6ee1f-0fce-40f7-9b1a-f0548eaa160a" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "ba2604d2-0be7-4ab4-afee-8b9e67313c10", "d5ce02f0-c382-46f7-af7d-216c21393524" });

            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "Id", "GameState", "NELat", "NELng", "Name", "SWLat", "SWLng" },
                values: new object[] { 1001, 2, 60.399819000000001, 5.3553319999999998, "The Walking Dead", 60.380307000000002, 5.295871 });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.392574639450814, 60.382494757850729, new DateTime(2020, 4, 5, 19, 25, 59, 807, DateTimeKind.Local).AddTicks(3031) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.383654089063263, 60.383153963338266, new DateTime(2020, 4, 5, 19, 25, 59, 809, DateTimeKind.Local).AddTicks(6463) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510001,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.383840722051211, 60.382856156069927, new DateTime(2020, 4, 5, 19, 25, 59, 809, DateTimeKind.Local).AddTicks(6671) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1000410003, "SeedCode0004", 1000, 10003, 60.382371010241627, 60.392435679870836, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 809, DateTimeKind.Local).AddTicks(6653), 10004 },
                    { 1000610005, "SeedCode0006", 1000, 10005, 60.398256658510441, 60.396892118571877, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 809, DateTimeKind.Local).AddTicks(6688), 10006 }
                });

            migrationBuilder.InsertData(
                table: "Players",
                columns: new[] { "Id", "BiteCode", "GameId", "IsGameAdmin", "IsHuman", "IsPatientZero", "PlayerName", "UserId" },
                values: new object[,]
                {
                    { 10011, "SeedCode0001", 1001, false, false, false, "Rick", "0001" },
                    { 10012, "SeedCode0002", 1001, true, false, false, "Daryl", "0002" },
                    { 10013, "SeedCode0003", 1001, false, false, true, "Carol", "0003" },
                    { 10014, "SeedCode0004", 1001, false, false, false, "Morgan", "0004" },
                    { 10015, "SeedCode0005", 1001, false, false, false, "Maggie", "0005" },
                    { 10016, "SeedCode0006", 1001, false, false, false, "Gabriel", "0006" }
                });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1001110013, "SeedCode0001", 1001, 10013, 60.392597436318553, 60.381768838114745, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 810, DateTimeKind.Local).AddTicks(1383), 10011 },
                    { 1001210013, "SeedCode0002", 1001, 10013, 60.38193814135272, 60.38096513042494, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 810, DateTimeKind.Local).AddTicks(1444), 10012 },
                    { 1001410013, "SeedCode0004", 1001, 10013, 60.382816381443199, 60.388734491794679, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 810, DateTimeKind.Local).AddTicks(1455), 10014 },
                    { 1001510011, "SeedCode0005", 1001, 10011, 60.393094089444432, 60.392502354867837, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 810, DateTimeKind.Local).AddTicks(1467), 10015 },
                    { 1001610014, "SeedCode0006", 1001, 10014, 60.386299203281332, 60.387432433544731, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 810, DateTimeKind.Local).AddTicks(1476), 10016 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610005);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610014);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10011);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10012);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10013);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10014);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10015);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10016);

            migrationBuilder.DeleteData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1001);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "56007540-859f-4bc0-b13a-e54d4652b5ca", "aaa3fb52-8858-47cd-985c-3820d6dab0b8" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "5b0a0bbd-e6ac-4b40-94ec-76f23d2a7a20", "ac5611be-88e1-4294-8eed-1cdf3a31a86e" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "e81a2792-3d76-442d-a6f4-49fe24ebefd8", "63b5ec61-b789-4d50-8234-e0c0849d39e7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "4da0a373-f18a-447b-909c-aaec903c86aa", "7902e141-e4b7-40ac-8033-9f866f6d0041" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "1511c765-22de-4541-9ed4-bd32ece407f7", "3a687e50-404d-4cdb-8977-c5cde7160a47" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "12620cc9-cc19-4688-82ed-d1ed2c2870e7", "b01e574a-0e28-4076-b5de-c98b8c7148a0" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.399277777128923, 60.398050455087329, new DateTime(2020, 4, 5, 19, 19, 52, 662, DateTimeKind.Local).AddTicks(8927) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.385840094582214, 60.397654787702017, new DateTime(2020, 4, 5, 19, 19, 52, 665, DateTimeKind.Local).AddTicks(2430) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510001,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.396960554132548, 60.386173079251535, new DateTime(2020, 4, 5, 19, 19, 52, 665, DateTimeKind.Local).AddTicks(2645) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1000410001, "SeedCode0004", 1000, 10001, 60.386787321725272, 60.399697614628373, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 19, 52, 665, DateTimeKind.Local).AddTicks(2626), 10004 },
                    { 1000610004, "SeedCode0006", 1000, 10004, 60.396180489469913, 60.399025863615229, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 19, 52, 665, DateTimeKind.Local).AddTicks(2661), 10006 }
                });
        }
    }
}

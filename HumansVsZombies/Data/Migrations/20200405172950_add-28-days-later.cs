﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class add28dayslater : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610005);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610014);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "21f6f6f9-ae2d-4b95-b407-b86619d28993", "974eaac5-e9cd-474c-8c52-054d61c2711e" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "27775c75-1fb7-45b4-ae25-54b309cd3d73", "e62d9afd-f3bf-4ac8-aa06-2fe694c9a230" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "3f8044cf-3568-4f23-a9d5-f4cb7ae90a12", "5441a2fe-fbde-4dd5-8a86-87311bf61fa9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "445e866f-063c-4ea4-a2bd-b4910b2606c0", "3add1822-4346-48b8-abb6-73d2c6e8b697" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "7fc1b3d8-7d90-4351-8205-afd3ecab8716", "430d46ac-2a00-425c-9c6d-56ee5856b55c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f464df0c-c260-4ff2-888c-af20f7e71ab4", "4b5cfaa7-62f7-466e-8208-19e78f66d368" });

            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "Id", "GameState", "NELat", "NELng", "Name", "SWLat", "SWLng" },
                values: new object[] { 1002, 2, 60.399819000000001, 5.3553319999999998, "28 Days Later", 60.380307000000002, 5.295871 });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.390759972389141, 60.382946056247903, new DateTime(2020, 4, 5, 19, 29, 49, 801, DateTimeKind.Local).AddTicks(6929) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.39134588846575, 60.385758094711925, new DateTime(2020, 4, 5, 19, 29, 49, 803, DateTimeKind.Local).AddTicks(9923) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.390669239062866, 60.389595661767146, new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(112) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.388375839114239, 60.382571617453323, new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(4750) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.383841100391543, 60.396632501834553, new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(4786) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1001610011, "SeedCode0006", 1001, 10011, 60.392890115694307, 60.395799055395152, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(4806), 10016 },
                    { 1001510013, "SeedCode0005", 1001, 10013, 60.398622013220958, 60.384910394082382, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(4798), 10015 },
                    { 1001210011, "SeedCode0002", 1001, 10011, 60.386812132646533, 60.396541580028639, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(4775), 10012 },
                    { 1000610002, "SeedCode0006", 1000, 10002, 60.383275937318054, 60.397064323463965, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(145), 10006 },
                    { 1000510003, "SeedCode0005", 1000, 10003, 60.393318695835667, 60.393530520393043, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(129), 10005 }
                });

            migrationBuilder.InsertData(
                table: "Players",
                columns: new[] { "Id", "BiteCode", "GameId", "IsGameAdmin", "IsHuman", "IsPatientZero", "PlayerName", "UserId" },
                values: new object[,]
                {
                    { 10021, "SeedCode0001", 1002, false, false, true, "Jim", "0001" },
                    { 10022, "SeedCode0002", 1002, false, false, false, "Selena", "0002" },
                    { 10023, "SeedCode0003", 1002, true, false, false, "Frank", "0003" },
                    { 10024, "SeedCode0004", 1002, false, false, false, "Hannah", "0004" },
                    { 10025, "SeedCode0005", 1002, false, false, false, "Henry West", "0005" },
                    { 10026, "SeedCode0006", 1002, false, false, false, "Private Clifton", "0006" }
                });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1002210021, "SeedCode0002", 1002, 10021, 60.392570634976423, 60.399573992678413, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(8215), 10022 },
                    { 1002310021, "SeedCode0003", 1002, 10021, 60.384236531568554, 60.398774945988876, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(8240), 10023 },
                    { 1002410022, "SeedCode0004", 1002, 10022, 60.390575170771939, 60.381870778837559, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(8250), 10024 },
                    { 1002510023, "SeedCode0005", 1002, 10023, 60.380869385025093, 60.390428335034819, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(8292), 10025 },
                    { 1002610023, "SeedCode0006", 1002, 10023, 60.397604011966912, 60.395232421982456, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(8303), 10026 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510023);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610023);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10021);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10022);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10023);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10024);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10025);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10026);

            migrationBuilder.DeleteData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1002);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "530594c5-8352-4f5e-a395-396a613a222b", "d7e42041-ef19-476b-bbda-64def65208ec" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "db382868-6ae7-4f91-a2c7-85d71e5998f1", "e38efcce-9817-4dd1-b6aa-abb46224facc" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "ed763ecf-6465-4813-b7f6-ca6ed0567785", "4e210712-39ec-41ea-8dc0-16b7a7ebd8c1" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "22cdbdba-47aa-42b5-a33a-f14c6f2e2048", "c33f6303-e28d-4b2c-beac-cf662f7032e7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a9b74724-08ed-4a04-9302-7b7d55aba1d6", "28a6ee1f-0fce-40f7-9b1a-f0548eaa160a" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "ba2604d2-0be7-4ab4-afee-8b9e67313c10", "d5ce02f0-c382-46f7-af7d-216c21393524" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.392574639450814, 60.382494757850729, new DateTime(2020, 4, 5, 19, 25, 59, 807, DateTimeKind.Local).AddTicks(3031) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.383654089063263, 60.383153963338266, new DateTime(2020, 4, 5, 19, 25, 59, 809, DateTimeKind.Local).AddTicks(6463) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.382371010241627, 60.392435679870836, new DateTime(2020, 4, 5, 19, 25, 59, 809, DateTimeKind.Local).AddTicks(6653) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.392597436318553, 60.381768838114745, new DateTime(2020, 4, 5, 19, 25, 59, 810, DateTimeKind.Local).AddTicks(1383) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.382816381443199, 60.388734491794679, new DateTime(2020, 4, 5, 19, 25, 59, 810, DateTimeKind.Local).AddTicks(1455) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1000510001, "SeedCode0005", 1000, 10001, 60.383840722051211, 60.382856156069927, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 809, DateTimeKind.Local).AddTicks(6671), 10005 },
                    { 1000610005, "SeedCode0006", 1000, 10005, 60.398256658510441, 60.396892118571877, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 809, DateTimeKind.Local).AddTicks(6688), 10006 },
                    { 1001210013, "SeedCode0002", 1001, 10013, 60.38193814135272, 60.38096513042494, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 810, DateTimeKind.Local).AddTicks(1444), 10012 },
                    { 1001510011, "SeedCode0005", 1001, 10011, 60.393094089444432, 60.392502354867837, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 810, DateTimeKind.Local).AddTicks(1467), 10015 },
                    { 1001610014, "SeedCode0006", 1001, 10014, 60.386299203281332, 60.387432433544731, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 25, 59, 810, DateTimeKind.Local).AddTicks(1476), 10016 }
                });
        }
    }
}

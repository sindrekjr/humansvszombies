﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class addresidentevil : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510023);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610023);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "1b8c7644-4f8d-4274-8d16-3a2e797c68e7", "04023007-05cc-4f64-a11a-65278118933b" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "31083db9-ca6e-4388-b909-6fbb4ef18580", "3d469ee1-d9c8-4fb9-bda4-0e0dc7de879c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "53ce8542-f00c-4ee4-8ded-2556947b3e81", "0828fcaa-7cdb-493f-bd77-74b4c29b8d5b" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "596099ee-3721-4de3-a402-c909f1e63eb8", "9e47974d-013c-47f1-b493-016ca5709476" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "8d502cab-aab9-4a10-9cdc-59b18a7d0685", "e5ed4d93-9445-4ae4-9ee7-ffeb6cbf8e6d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "e54c28db-e999-491e-a7b4-5b06df1d2b7c", "b7c5bc42-0380-4f3c-b556-31d032667a67" });

            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "Id", "GameState", "NELat", "NELng", "Name", "SWLat", "SWLng" },
                values: new object[] { 1003, 2, 60.399819000000001, 5.3553319999999998, "Resident Evil", 60.380307000000002, 5.295871 });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38779035164162, 60.396407658262824, new DateTime(2020, 4, 5, 19, 33, 42, 919, DateTimeKind.Local).AddTicks(9939) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.385119446024781, 60.391010583045556, new DateTime(2020, 4, 5, 19, 33, 42, 922, DateTimeKind.Local).AddTicks(8722) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.382325441074642, 60.395522611380898, new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(4425) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.387924655827376, 60.38752455259246, new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(4454) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.388177946240035, 60.398138641612562, new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(4465) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38149227565561, 60.39496099878248, new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(8421) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.396419396607861, 60.390025241094065, new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(8450) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1002510021, "SeedCode0005", 1002, 10021, 60.382270940322719, 60.386627770091287, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(8468), 10025 },
                    { 1002610022, "SeedCode0006", 1002, 10022, 60.394307642741786, 60.398257055349838, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(8477), 10026 },
                    { 1001610014, "SeedCode0006", 1001, 10014, 60.383441454272429, 60.391105619947709, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(4486), 10016 },
                    { 1001510012, "SeedCode0005", 1001, 10012, 60.391048030315908, 60.397355637425441, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(4477), 10015 },
                    { 1000610005, "SeedCode0006", 1000, 10005, 60.381623557116534, 60.393542449607331, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 922, DateTimeKind.Local).AddTicks(9219), 10006 },
                    { 1000510002, "SeedCode0005", 1000, 10002, 60.385295340743667, 60.399362103901304, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 922, DateTimeKind.Local).AddTicks(9195), 10005 },
                    { 1000410002, "SeedCode0004", 1000, 10002, 60.395181392821108, 60.399388217316407, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 922, DateTimeKind.Local).AddTicks(9157), 10004 },
                    { 1002410021, "SeedCode0004", 1002, 10021, 60.383068428175577, 60.398909999295768, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(8460), 10024 }
                });

            migrationBuilder.InsertData(
                table: "Players",
                columns: new[] { "Id", "BiteCode", "GameId", "IsGameAdmin", "IsHuman", "IsPatientZero", "PlayerName", "UserId" },
                values: new object[,]
                {
                    { 10031, "SeedCode0001", 1003, false, false, false, "Alice", "0001" },
                    { 10032, "SeedCode0002", 1003, false, false, false, "Nemesis", "0002" },
                    { 10033, "SeedCode0003", 1003, false, false, false, "Tyrant", "0003" },
                    { 10034, "SeedCode0004", 1003, true, false, false, "Red Queen", "0004" },
                    { 10035, "SeedCode0005", 1003, false, false, true, "T", "0005" },
                    { 10036, "SeedCode0006", 1003, false, false, false, "Valentine", "0006" }
                });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1003310032, "SeedCode0003", 1003, 10032, 60.396371722109571, 60.392127479028375, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 924, DateTimeKind.Local).AddTicks(2496), 10033 },
                    { 1003410032, "SeedCode0004", 1003, 10032, 60.388242702640845, 60.394121361817511, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 924, DateTimeKind.Local).AddTicks(2509), 10034 },
                    { 1003110035, "SeedCode0001", 1003, 10035, 60.393722686281279, 60.395013012609979, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 924, DateTimeKind.Local).AddTicks(2422), 10031 },
                    { 1003210035, "SeedCode0002", 1003, 10035, 60.385215082910733, 60.389935931486811, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 924, DateTimeKind.Local).AddTicks(2485), 10032 },
                    { 1003610033, "SeedCode0006", 1003, 10033, 60.386547323760546, 60.397067867206331, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 924, DateTimeKind.Local).AddTicks(2519), 10036 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610005);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510012);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610014);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310032);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410032);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610033);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10031);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10032);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10033);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10034);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10035);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10036);

            migrationBuilder.DeleteData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1003);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "21f6f6f9-ae2d-4b95-b407-b86619d28993", "974eaac5-e9cd-474c-8c52-054d61c2711e" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "27775c75-1fb7-45b4-ae25-54b309cd3d73", "e62d9afd-f3bf-4ac8-aa06-2fe694c9a230" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "3f8044cf-3568-4f23-a9d5-f4cb7ae90a12", "5441a2fe-fbde-4dd5-8a86-87311bf61fa9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "445e866f-063c-4ea4-a2bd-b4910b2606c0", "3add1822-4346-48b8-abb6-73d2c6e8b697" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "7fc1b3d8-7d90-4351-8205-afd3ecab8716", "430d46ac-2a00-425c-9c6d-56ee5856b55c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f464df0c-c260-4ff2-888c-af20f7e71ab4", "4b5cfaa7-62f7-466e-8208-19e78f66d368" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.390759972389141, 60.382946056247903, new DateTime(2020, 4, 5, 19, 29, 49, 801, DateTimeKind.Local).AddTicks(6929) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.39134588846575, 60.385758094711925, new DateTime(2020, 4, 5, 19, 29, 49, 803, DateTimeKind.Local).AddTicks(9923) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.388375839114239, 60.382571617453323, new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(4750) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.386812132646533, 60.396541580028639, new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(4775) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.383841100391543, 60.396632501834553, new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(4786) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.392570634976423, 60.399573992678413, new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(8215) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.384236531568554, 60.398774945988876, new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(8240) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1002510023, "SeedCode0005", 1002, 10023, 60.380869385025093, 60.390428335034819, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(8292), 10025 },
                    { 1002410022, "SeedCode0004", 1002, 10022, 60.390575170771939, 60.381870778837559, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(8250), 10024 },
                    { 1001610011, "SeedCode0006", 1001, 10011, 60.392890115694307, 60.395799055395152, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(4806), 10016 },
                    { 1001510013, "SeedCode0005", 1001, 10013, 60.398622013220958, 60.384910394082382, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(4798), 10015 },
                    { 1000610002, "SeedCode0006", 1000, 10002, 60.383275937318054, 60.397064323463965, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(145), 10006 },
                    { 1000510003, "SeedCode0005", 1000, 10003, 60.393318695835667, 60.393530520393043, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(129), 10005 },
                    { 1002610023, "SeedCode0006", 1002, 10023, 60.397604011966912, 60.395232421982456, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(8303), 10026 },
                    { 1000410003, "SeedCode0004", 1000, 10003, 60.390669239062866, 60.389595661767146, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 29, 49, 804, DateTimeKind.Local).AddTicks(112), 10004 }
                });
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class addevildead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510012);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310032);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410032);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610033);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "d37005bc-f902-4b98-a5f2-ec5185fa3c34", "ab40d9de-ad90-4e07-86cd-65699b7a7327" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "0dd3c3b0-683d-473a-8123-e50bca6cd5de", "6f21d2a4-5ac9-4e7b-815f-93cd2994c8b3" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "bc6e9d3a-6411-47e8-8773-35d89024e408", "c0a748a0-2f43-4c94-858b-9fc0038f1076" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a04f2d14-5c56-4aaa-8611-92a7d2039326", "894198b7-318a-4d6b-800b-f0769dddddca" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "7ea0806a-1ca0-4a5b-800e-f6bf63a75960", "5826c14c-a749-4f51-aaba-6aab8a5c32f9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "4232c7ed-5090-4988-a3a0-f18cebb76db2", "7ca60408-9578-4d44-bfe4-1218e65d5b66" });

            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "Id", "GameState", "NELat", "NELng", "Name", "SWLat", "SWLng" },
                values: new object[] { 1004, 2, 60.399819000000001, 5.3553319999999998, "Evil Dead", 60.380307000000002, 5.295871 });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.397656104406337, 60.383160724645293, new DateTime(2020, 4, 5, 19, 36, 12, 584, DateTimeKind.Local).AddTicks(5162) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610005,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.389331506017967, 60.393199014839993, new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(30) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.393263403058604, 60.388428012518858, new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(4839) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610014,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.3912767812147, 60.390236471618962, new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(4911) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.391552254182692, 60.388932224546608, new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(8916) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.397085330801914, 60.398476472390882, new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(8945) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.39935658270376, 60.398105460583778, new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(8957) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.397960621118912, 60.381654397741457, new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(2498) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.396240081089047, 60.391337306745683, new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(2527) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1003410031, "SeedCode0004", 1003, 10031, 60.388821440463758, 60.393278436926039, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(2553), 10034 },
                    { 1001410011, "SeedCode0004", 1001, 10011, 60.38658917667869, 60.392017333517501, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(4886), 10014 },
                    { 1003310035, "SeedCode0003", 1003, 10035, 60.388488073438097, 60.381009780662865, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(2540), 10033 },
                    { 1002610024, "SeedCode0006", 1002, 10024, 60.391160406153958, 60.391745369556155, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(8980), 10026 },
                    { 1001510011, "SeedCode0005", 1001, 10011, 60.38329106299026, 60.397225748018897, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(4900), 10015 },
                    { 1001210013, "SeedCode0002", 1001, 10013, 60.399757702378523, 60.380964513023308, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(4873), 10012 },
                    { 1000510004, "SeedCode0005", 1000, 10004, 60.393223822662932, 60.398499889458741, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(9), 10005 },
                    { 1000410001, "SeedCode0004", 1000, 10001, 60.386221376721167, 60.390894582817957, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 586, DateTimeKind.Local).AddTicks(9988), 10004 },
                    { 1000310002, "SeedCode0003", 1000, 10002, 60.389357380862407, 60.385249953986879, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 586, DateTimeKind.Local).AddTicks(9787), 10003 },
                    { 1003610034, "SeedCode0006", 1003, 10034, 60.395124902129872, 60.393453308072594, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(2596), 10036 },
                    { 1002510023, "SeedCode0005", 1002, 10023, 60.39672479934007, 60.382902523404184, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(8969), 10025 }
                });

            migrationBuilder.InsertData(
                table: "Players",
                columns: new[] { "Id", "BiteCode", "GameId", "IsGameAdmin", "IsHuman", "IsPatientZero", "PlayerName", "UserId" },
                values: new object[,]
                {
                    { 10041, "SeedCode0001", 1004, false, false, false, "Ash", "0001" },
                    { 10042, "SeedCode0002", 1004, false, false, false, "Linda", "0002" },
                    { 10043, "SeedCode0003", 1004, false, false, false, "Scotty", "0003" },
                    { 10044, "SeedCode0004", 1004, true, false, true, "Henry the Red", "0004" }
                });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[] { 1004310042, "SeedCode0003", 1004, 10042, 60.392663771626992, 60.386409499801246, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(5529), 10043 });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[] { 1004110044, "SeedCode0001", 1004, 10044, 60.395692168363119, 60.380581977754666, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(5488), 10041 });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[] { 1004210044, "SeedCode0002", 1004, 10044, 60.39973180351074, 60.387461696815748, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(5517), 10042 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510004);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510023);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610024);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610034);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004110044);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004210044);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004310042);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10041);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10042);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10043);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10044);

            migrationBuilder.DeleteData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1004);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "1b8c7644-4f8d-4274-8d16-3a2e797c68e7", "04023007-05cc-4f64-a11a-65278118933b" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "31083db9-ca6e-4388-b909-6fbb4ef18580", "3d469ee1-d9c8-4fb9-bda4-0e0dc7de879c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "53ce8542-f00c-4ee4-8ded-2556947b3e81", "0828fcaa-7cdb-493f-bd77-74b4c29b8d5b" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "596099ee-3721-4de3-a402-c909f1e63eb8", "9e47974d-013c-47f1-b493-016ca5709476" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "8d502cab-aab9-4a10-9cdc-59b18a7d0685", "e5ed4d93-9445-4ae4-9ee7-ffeb6cbf8e6d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "e54c28db-e999-491e-a7b4-5b06df1d2b7c", "b7c5bc42-0380-4f3c-b556-31d032667a67" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38779035164162, 60.396407658262824, new DateTime(2020, 4, 5, 19, 33, 42, 919, DateTimeKind.Local).AddTicks(9939) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610005,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.381623557116534, 60.393542449607331, new DateTime(2020, 4, 5, 19, 33, 42, 922, DateTimeKind.Local).AddTicks(9219) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.382325441074642, 60.395522611380898, new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(4425) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610014,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.383441454272429, 60.391105619947709, new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(4486) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38149227565561, 60.39496099878248, new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(8421) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.396419396607861, 60.390025241094065, new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(8450) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.383068428175577, 60.398909999295768, new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(8460) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.393722686281279, 60.395013012609979, new DateTime(2020, 4, 5, 19, 33, 42, 924, DateTimeKind.Local).AddTicks(2422) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.385215082910733, 60.389935931486811, new DateTime(2020, 4, 5, 19, 33, 42, 924, DateTimeKind.Local).AddTicks(2485) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1000410002, "SeedCode0004", 1000, 10002, 60.395181392821108, 60.399388217316407, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 922, DateTimeKind.Local).AddTicks(9157), 10004 },
                    { 1000510002, "SeedCode0005", 1000, 10002, 60.385295340743667, 60.399362103901304, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 922, DateTimeKind.Local).AddTicks(9195), 10005 },
                    { 1001210011, "SeedCode0002", 1001, 10011, 60.387924655827376, 60.38752455259246, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(4454), 10012 },
                    { 1001410013, "SeedCode0004", 1001, 10013, 60.388177946240035, 60.398138641612562, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(4465), 10014 },
                    { 1003310032, "SeedCode0003", 1003, 10032, 60.396371722109571, 60.392127479028375, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 924, DateTimeKind.Local).AddTicks(2496), 10033 },
                    { 1001510012, "SeedCode0005", 1001, 10012, 60.391048030315908, 60.397355637425441, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(4477), 10015 },
                    { 1002510021, "SeedCode0005", 1002, 10021, 60.382270940322719, 60.386627770091287, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(8468), 10025 },
                    { 1003610033, "SeedCode0006", 1003, 10033, 60.386547323760546, 60.397067867206331, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 924, DateTimeKind.Local).AddTicks(2519), 10036 },
                    { 1003410032, "SeedCode0004", 1003, 10032, 60.388242702640845, 60.394121361817511, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 924, DateTimeKind.Local).AddTicks(2509), 10034 },
                    { 1002610022, "SeedCode0006", 1002, 10022, 60.394307642741786, 60.398257055349838, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 923, DateTimeKind.Local).AddTicks(8477), 10026 },
                    { 1000310001, "SeedCode0003", 1000, 10001, 60.385119446024781, 60.391010583045556, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 33, 42, 922, DateTimeKind.Local).AddTicks(8722), 10003 }
                });
        }
    }
}

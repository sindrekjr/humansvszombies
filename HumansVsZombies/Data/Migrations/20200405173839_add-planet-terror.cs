﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class addplanetterror : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610005);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610014);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510023);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610024);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610034);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004210044);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004310042);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "976f1130-cebc-4a39-ab89-bba48de55f2d", "1af1defa-3454-4fa1-b99c-e4b57e615e7f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "724849cd-50b5-4ab8-b565-dcc289633023", "1d911a07-5535-442a-a0fc-e05273c32733" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "04c98823-0318-4934-8ddd-ada1de2f4274", "c94c14ac-9c64-4bfa-be73-8031196d2931" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "b2143343-6c46-4872-af89-577c2546b253", "e4307159-6ff2-41e0-91cd-59517319bac0" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "46840795-f7d9-4aee-b58c-229c70dd8cad", "387194e4-637c-4d62-91f1-692ed9bfe191" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "065b5b09-3974-4ddf-b171-e6338827f270", "ee8b145c-0179-4114-89b1-609648210d47" });

            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "Id", "GameState", "NELat", "NELng", "Name", "SWLat", "SWLng" },
                values: new object[] { 1005, 2, 60.399819000000001, 5.3553319999999998, "Planet Terror", 60.380307000000002, 5.295871 });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.381577753876449, 60.380820226713084, new DateTime(2020, 4, 5, 19, 38, 38, 228, DateTimeKind.Local).AddTicks(8872) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.397775737573397, 60.390579492174503, new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(1544) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510004,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.385814543425916, 60.385117581426599, new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(1752) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38625000957942, 60.393700387498043, new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(6304) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.3874594464449, 60.396768213348224, new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(9754) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38984490474688, 60.396267432060469, new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(9790) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.386866977829897, 60.384304165636493, new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(3158) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.381153486960272, 60.382103431493704, new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(3183) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004110044,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.381509706550744, 60.387335638768363, new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(6019) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1000410002, "SeedCode0004", 1000, 10002, 60.386604500334471, 60.383651744143677, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(1732), 10004 },
                    { 1000610003, "SeedCode0006", 1000, 10003, 60.397309598131514, 60.394812124096248, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(1768), 10006 },
                    { 1001210011, "SeedCode0002", 1001, 10011, 60.396727808799376, 60.392467138042775, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(6332), 10012 },
                    { 1001410013, "SeedCode0004", 1001, 10013, 60.39427793004171, 60.382903377477504, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(6342), 10014 },
                    { 1002310022, "SeedCode0003", 1002, 10022, 60.397877264874893, 60.3820668017256, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(9780), 10023 },
                    { 1001510014, "SeedCode0005", 1001, 10014, 60.382555569272171, 60.39594555136194, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(6354), 10015 },
                    { 1004210041, "SeedCode0002", 1004, 10041, 60.39049792825876, 60.389533842692018, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(6043), 10042 },
                    { 1003610035, "SeedCode0006", 1003, 10035, 60.381009131687435, 60.387293734541664, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(3247), 10036 },
                    { 1003410035, "SeedCode0004", 1003, 10035, 60.389966848306791, 60.387316859476996, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(3237), 10034 },
                    { 1003310032, "SeedCode0003", 1003, 10032, 60.389314562467703, 60.388769781880022, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(3223), 10033 },
                    { 1002610025, "SeedCode0006", 1002, 10025, 60.391970827930926, 60.39019061836963, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(9808), 10026 },
                    { 1002510024, "SeedCode0005", 1002, 10024, 60.397443434888878, 60.389078243553797, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(9799), 10025 },
                    { 1001610015, "SeedCode0006", 1001, 10015, 60.383317710025139, 60.396058540022281, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(6363), 10016 },
                    { 1004310041, "SeedCode0003", 1004, 10041, 60.384658909608802, 60.390987449424024, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(6053), 10043 }
                });

            migrationBuilder.InsertData(
                table: "Players",
                columns: new[] { "Id", "BiteCode", "GameId", "IsGameAdmin", "IsHuman", "IsPatientZero", "PlayerName", "UserId" },
                values: new object[,]
                {
                    { 10051, "SeedCode0001", 1005, false, false, false, "Cherry", "0001" },
                    { 10052, "SeedCode0002", 1005, true, false, false, "Muldoon", "0002" },
                    { 10053, "SeedCode0003", 1005, false, false, true, "El Wray", "0003" },
                    { 10054, "SeedCode0004", 1005, false, false, false, "Dakota", "0004" }
                });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[] { 1005110053, "SeedCode0001", 1005, 10053, 60.3873817396289, 60.382962897544516, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(8735), 10051 });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[] { 1005210053, "SeedCode0002", 1005, 10053, 60.389517080098194, 60.394240352378418, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(8759), 10052 });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[] { 1005410052, "SeedCode0004", 1005, 10052, 60.3951975963544, 60.385393867626419, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(8770), 10054 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510014);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610015);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510024);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610025);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310032);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004210041);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004310041);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1005110053);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1005210053);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1005410052);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10051);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10052);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10053);

            migrationBuilder.DeleteData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10054);

            migrationBuilder.DeleteData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1005);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "d37005bc-f902-4b98-a5f2-ec5185fa3c34", "ab40d9de-ad90-4e07-86cd-65699b7a7327" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "0dd3c3b0-683d-473a-8123-e50bca6cd5de", "6f21d2a4-5ac9-4e7b-815f-93cd2994c8b3" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "bc6e9d3a-6411-47e8-8773-35d89024e408", "c0a748a0-2f43-4c94-858b-9fc0038f1076" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a04f2d14-5c56-4aaa-8611-92a7d2039326", "894198b7-318a-4d6b-800b-f0769dddddca" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "7ea0806a-1ca0-4a5b-800e-f6bf63a75960", "5826c14c-a749-4f51-aaba-6aab8a5c32f9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "4232c7ed-5090-4988-a3a0-f18cebb76db2", "7ca60408-9578-4d44-bfe4-1218e65d5b66" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.397656104406337, 60.383160724645293, new DateTime(2020, 4, 5, 19, 36, 12, 584, DateTimeKind.Local).AddTicks(5162) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.389357380862407, 60.385249953986879, new DateTime(2020, 4, 5, 19, 36, 12, 586, DateTimeKind.Local).AddTicks(9787) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510004,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.393223822662932, 60.398499889458741, new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(9) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.393263403058604, 60.388428012518858, new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(4839) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.391552254182692, 60.388932224546608, new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(8916) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.39935658270376, 60.398105460583778, new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(8957) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.397960621118912, 60.381654397741457, new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(2498) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.396240081089047, 60.391337306745683, new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(2527) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004110044,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.395692168363119, 60.380581977754666, new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(5488) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1000610005, "SeedCode0006", 1000, 10005, 60.389331506017967, 60.393199014839993, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(30), 10006 },
                    { 1001210013, "SeedCode0002", 1001, 10013, 60.399757702378523, 60.380964513023308, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(4873), 10012 },
                    { 1001410011, "SeedCode0004", 1001, 10011, 60.38658917667869, 60.392017333517501, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(4886), 10014 },
                    { 1001510011, "SeedCode0005", 1001, 10011, 60.38329106299026, 60.397225748018897, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(4900), 10015 },
                    { 1002510023, "SeedCode0005", 1002, 10023, 60.39672479934007, 60.382902523404184, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(8969), 10025 },
                    { 1001610014, "SeedCode0006", 1001, 10014, 60.3912767812147, 60.390236471618962, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(4911), 10016 },
                    { 1004310042, "SeedCode0003", 1004, 10042, 60.392663771626992, 60.386409499801246, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(5529), 10043 },
                    { 1004210044, "SeedCode0002", 1004, 10044, 60.39973180351074, 60.387461696815748, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(5517), 10042 },
                    { 1003610034, "SeedCode0006", 1003, 10034, 60.395124902129872, 60.393453308072594, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(2596), 10036 },
                    { 1003410031, "SeedCode0004", 1003, 10031, 60.388821440463758, 60.393278436926039, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(2553), 10034 },
                    { 1003310035, "SeedCode0003", 1003, 10035, 60.388488073438097, 60.381009780662865, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 588, DateTimeKind.Local).AddTicks(2540), 10033 },
                    { 1002610024, "SeedCode0006", 1002, 10024, 60.391160406153958, 60.391745369556155, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(8980), 10026 },
                    { 1002310021, "SeedCode0003", 1002, 10021, 60.397085330801914, 60.398476472390882, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 587, DateTimeKind.Local).AddTicks(8945), 10023 },
                    { 1000410001, "SeedCode0004", 1000, 10001, 60.386221376721167, 60.390894582817957, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 36, 12, 586, DateTimeKind.Local).AddTicks(9988), 10004 }
                });
        }
    }
}

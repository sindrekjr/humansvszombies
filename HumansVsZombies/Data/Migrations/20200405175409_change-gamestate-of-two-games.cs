﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class changegamestateoftwogames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510004);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510014);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610015);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510024);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310032);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004110044);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004210041);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1004310041);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1005110053);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1005210053);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1005410052);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "6027eb4f-3258-463c-a331-1b9d84b88ebb", "e4003054-b423-4154-bac4-eb892973f435" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "778d98d8-2c90-4f29-9370-010f112a2236", "5f692354-f581-4691-a1bf-18ad03fadff8" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "eb012962-7b05-42b2-88d6-b05b9eca65d4", "d0f0879f-a9a7-49aa-912c-016c48e5cefb" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "2c17f912-3eb9-4130-b272-ca8913fc6c97", "51c29482-33e6-4266-aa52-8c2eef235876" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "e72c7170-5935-4f30-8c00-67759e22c9e2", "52904f92-cd7e-4a15-a4ba-4b75d2c84ee4" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f0096781-99ce-48fe-be04-7fabcbc61f7b", "f9f20b1d-a6f1-4a1f-8eef-b3714650472e" });

            migrationBuilder.UpdateData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1004,
                column: "GameState",
                value: 0);

            migrationBuilder.UpdateData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1005,
                column: "GameState",
                value: 0);

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.394315252298014, 60.399484546327116, new DateTime(2020, 4, 5, 19, 54, 8, 641, DateTimeKind.Local).AddTicks(2830) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38417622092107, 60.394200217301652, new DateTime(2020, 4, 5, 19, 54, 8, 643, DateTimeKind.Local).AddTicks(5889) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.385402329193447, 60.397590069725823, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(775) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.382966325849466, 60.383387171629536, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(4253) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.387911788830614, 60.389456805867326, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(4288) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610025,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.399525794418629, 60.389318175545206, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(4306) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.392779461177568, 60.380607730183655, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(7669) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.385247739333025, 60.396234759774046, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(7746) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1000410003, "SeedCode0004", 1000, 10003, 60.393670197293211, 60.386691111159436, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 643, DateTimeKind.Local).AddTicks(6096), 10004 },
                    { 1002510022, "SeedCode0005", 1002, 10022, 60.395663043571574, 60.383664536426402, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(4297), 10025 },
                    { 1000510002, "SeedCode0005", 1000, 10002, 60.399320696459817, 60.388522976060777, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 643, DateTimeKind.Local).AddTicks(6115), 10005 },
                    { 1000610001, "SeedCode0006", 1000, 10001, 60.387395040390501, 60.395885708197362, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 643, DateTimeKind.Local).AddTicks(6130), 10006 },
                    { 1003610034, "SeedCode0006", 1003, 10034, 60.389786542279538, 60.394125468300238, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(7756), 10036 },
                    { 1003310031, "SeedCode0003", 1003, 10031, 60.389058447995232, 60.380603796461813, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(7731), 10033 },
                    { 1003210031, "SeedCode0002", 1003, 10031, 60.390280906264671, 60.397758926460419, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(7693), 10032 },
                    { 1001210013, "SeedCode0002", 1001, 10013, 60.393031373346538, 60.398662384851292, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(805), 10012 },
                    { 1002310021, "SeedCode0003", 1002, 10021, 60.391460843669009, 60.398584163014519, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(4278), 10023 },
                    { 1001610014, "SeedCode0006", 1001, 10014, 60.395612670917927, 60.390885259799795, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(835), 10016 },
                    { 1001510011, "SeedCode0005", 1001, 10011, 60.397769636199492, 60.390475563347742, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(825), 10015 },
                    { 1001410011, "SeedCode0004", 1001, 10011, 60.390747669023163, 60.387793465208119, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(814), 10014 }
                });

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10041,
                column: "IsHuman",
                value: true);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10042,
                column: "IsHuman",
                value: true);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10043,
                column: "IsHuman",
                value: true);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10044,
                column: "IsHuman",
                value: true);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10051,
                column: "IsHuman",
                value: true);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10052,
                column: "IsHuman",
                value: true);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10053,
                column: "IsHuman",
                value: true);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10054,
                column: "IsHuman",
                value: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610014);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610034);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "976f1130-cebc-4a39-ab89-bba48de55f2d", "1af1defa-3454-4fa1-b99c-e4b57e615e7f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "724849cd-50b5-4ab8-b565-dcc289633023", "1d911a07-5535-442a-a0fc-e05273c32733" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "04c98823-0318-4934-8ddd-ada1de2f4274", "c94c14ac-9c64-4bfa-be73-8031196d2931" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "b2143343-6c46-4872-af89-577c2546b253", "e4307159-6ff2-41e0-91cd-59517319bac0" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "46840795-f7d9-4aee-b58c-229c70dd8cad", "387194e4-637c-4d62-91f1-692ed9bfe191" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "065b5b09-3974-4ddf-b171-e6338827f270", "ee8b145c-0179-4114-89b1-609648210d47" });

            migrationBuilder.UpdateData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1004,
                column: "GameState",
                value: 2);

            migrationBuilder.UpdateData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1005,
                column: "GameState",
                value: 2);

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.381577753876449, 60.380820226713084, new DateTime(2020, 4, 5, 19, 38, 38, 228, DateTimeKind.Local).AddTicks(8872) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.397775737573397, 60.390579492174503, new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(1544) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38625000957942, 60.393700387498043, new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(6304) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.3874594464449, 60.396768213348224, new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(9754) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38984490474688, 60.396267432060469, new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(9790) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610025,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.391970827930926, 60.39019061836963, new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(9808) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.386866977829897, 60.384304165636493, new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(3158) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.389966848306791, 60.387316859476996, new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(3237) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1005410052, "SeedCode0004", 1005, 10052, 60.3951975963544, 60.385393867626419, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(8770), 10054 },
                    { 1000410002, "SeedCode0004", 1000, 10002, 60.386604500334471, 60.383651744143677, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(1732), 10004 },
                    { 1003610035, "SeedCode0006", 1003, 10035, 60.381009131687435, 60.387293734541664, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(3247), 10036 },
                    { 1000510004, "SeedCode0005", 1000, 10004, 60.385814543425916, 60.385117581426599, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(1752), 10005 },
                    { 1005110053, "SeedCode0001", 1005, 10053, 60.3873817396289, 60.382962897544516, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(8735), 10051 },
                    { 1000610003, "SeedCode0006", 1000, 10003, 60.397309598131514, 60.394812124096248, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(1768), 10006 },
                    { 1001210011, "SeedCode0002", 1001, 10011, 60.396727808799376, 60.392467138042775, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(6332), 10012 },
                    { 1001410013, "SeedCode0004", 1001, 10013, 60.39427793004171, 60.382903377477504, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(6342), 10014 },
                    { 1001510014, "SeedCode0005", 1001, 10014, 60.382555569272171, 60.39594555136194, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(6354), 10015 },
                    { 1001610015, "SeedCode0006", 1001, 10015, 60.383317710025139, 60.396058540022281, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(6363), 10016 },
                    { 1004310041, "SeedCode0003", 1004, 10041, 60.384658909608802, 60.390987449424024, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(6053), 10043 },
                    { 1004210041, "SeedCode0002", 1004, 10041, 60.39049792825876, 60.389533842692018, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(6043), 10042 },
                    { 1004110044, "SeedCode0001", 1004, 10044, 60.381509706550744, 60.387335638768363, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(6019), 10041 },
                    { 1005210053, "SeedCode0002", 1005, 10053, 60.389517080098194, 60.394240352378418, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(8759), 10052 },
                    { 1003310032, "SeedCode0003", 1003, 10032, 60.389314562467703, 60.388769781880022, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(3223), 10033 },
                    { 1003210035, "SeedCode0002", 1003, 10035, 60.381153486960272, 60.382103431493704, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 232, DateTimeKind.Local).AddTicks(3183), 10032 },
                    { 1002510024, "SeedCode0005", 1002, 10024, 60.397443434888878, 60.389078243553797, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(9799), 10025 },
                    { 1002310022, "SeedCode0003", 1002, 10022, 60.397877264874893, 60.3820668017256, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 38, 38, 231, DateTimeKind.Local).AddTicks(9780), 10023 }
                });

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10041,
                column: "IsHuman",
                value: false);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10042,
                column: "IsHuman",
                value: false);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10043,
                column: "IsHuman",
                value: false);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10044,
                column: "IsHuman",
                value: false);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10051,
                column: "IsHuman",
                value: false);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10052,
                column: "IsHuman",
                value: false);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10053,
                column: "IsHuman",
                value: false);

            migrationBuilder.UpdateData(
                table: "Players",
                keyColumn: "Id",
                keyValue: 10054,
                column: "IsHuman",
                value: false);
        }
    }
}

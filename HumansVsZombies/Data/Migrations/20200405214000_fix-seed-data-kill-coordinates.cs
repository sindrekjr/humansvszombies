﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class fixseeddatakillcoordinates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610014);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610025);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610034);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "8afade78-6878-4e36-b1b5-b8afffbcda84", "dd83f601-4e65-4dba-85dc-f46be06b8755" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a954bb93-b591-467d-8cbd-9704f7ff5e75", "53542d9f-737f-4530-9ee3-d3a7403472fe" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "601879b8-94da-4138-83ab-0380cae84d36", "94650ec8-7bde-45ab-9632-1ac2f7192028" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "9e3c3828-61e1-4353-9bfc-1f8b8f4ee76b", "dcadc361-8fc9-408c-8f21-afd7e1015477" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "32337f43-c8e3-428c-8632-a671b9b670eb", "a4b87659-8277-41b6-95a8-3b2c38f99b44" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "39b671e9-6688-4b55-b7e2-f98d5009ee6b", "e172b4b0-173e-4a6d-b55b-be984e0a67d0" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.386817482818614, 5.3231979326388972, new DateTime(2020, 4, 5, 23, 39, 59, 902, DateTimeKind.Local).AddTicks(6885) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.391372699021076, 5.3042746112285579, new DateTime(2020, 4, 5, 23, 39, 59, 905, DateTimeKind.Local).AddTicks(9165) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.399376952978017, 5.3212616466694866, new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(4392) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.398233160958945, 5.3120602471234042, new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(9079) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.395316877348755, 5.3044241557440071, new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(9142) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.393922093752515, 5.3327639149534507, new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(9153) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.380531860662465, 5.3021714176672194, new DateTime(2020, 4, 5, 23, 39, 59, 907, DateTimeKind.Local).AddTicks(2548) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1003610032, "SeedCode0006", 1003, 10032, 60.398560218777924, 5.3143496442449312, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 907, DateTimeKind.Local).AddTicks(2607), 10036 },
                    { 1003310032, "SeedCode0003", 1003, 10032, 60.393533855957365, 5.3298688647109955, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 907, DateTimeKind.Local).AddTicks(2584), 10033 },
                    { 1003210035, "SeedCode0002", 1003, 10035, 60.38729060776415, 5.3435949890035719, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 907, DateTimeKind.Local).AddTicks(2575), 10032 },
                    { 1002610022, "SeedCode0006", 1002, 10022, 60.391016652218738, 5.3164598352078842, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(9171), 10026 },
                    { 1002510024, "SeedCode0005", 1002, 10024, 60.386043015594005, 5.3041543157948157, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(9162), 10025 },
                    { 1001610013, "SeedCode0006", 1001, 10013, 60.385719555842485, 5.3504532234613551, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(4451), 10016 },
                    { 1001510013, "SeedCode0005", 1001, 10013, 60.399476848318841, 5.3384428784804721, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(4442), 10015 },
                    { 1001410012, "SeedCode0004", 1001, 10012, 60.395427448447073, 5.3194616881597936, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(4431), 10014 },
                    { 1001210011, "SeedCode0002", 1001, 10011, 60.380965773276415, 5.3256968759626151, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(4420), 10012 },
                    { 1000610004, "SeedCode0006", 1000, 10004, 60.383253438470554, 5.351175750358987, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 905, DateTimeKind.Local).AddTicks(9203), 10006 },
                    { 1000510003, "SeedCode0005", 1000, 10003, 60.396275886885697, 5.3074523406638177, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 905, DateTimeKind.Local).AddTicks(9185), 10005 },
                    { 1003410031, "SeedCode0004", 1003, 10031, 60.385076998492643, 5.3189907954297206, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 907, DateTimeKind.Local).AddTicks(2597), 10034 },
                    { 1000310001, "SeedCode0003", 1000, 10001, 60.391440620405966, 5.3254789943861338, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 905, DateTimeKind.Local).AddTicks(8861), 10003 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610004);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410012);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510024);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310032);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610032);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "6027eb4f-3258-463c-a331-1b9d84b88ebb", "e4003054-b423-4154-bac4-eb892973f435" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "778d98d8-2c90-4f29-9370-010f112a2236", "5f692354-f581-4691-a1bf-18ad03fadff8" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "eb012962-7b05-42b2-88d6-b05b9eca65d4", "d0f0879f-a9a7-49aa-912c-016c48e5cefb" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "2c17f912-3eb9-4130-b272-ca8913fc6c97", "51c29482-33e6-4266-aa52-8c2eef235876" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "e72c7170-5935-4f30-8c00-67759e22c9e2", "52904f92-cd7e-4a15-a4ba-4b75d2c84ee4" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f0096781-99ce-48fe-be04-7fabcbc61f7b", "f9f20b1d-a6f1-4a1f-8eef-b3714650472e" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.394315252298014, 60.399484546327116, new DateTime(2020, 4, 5, 19, 54, 8, 641, DateTimeKind.Local).AddTicks(2830) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.393670197293211, 60.386691111159436, new DateTime(2020, 4, 5, 19, 54, 8, 643, DateTimeKind.Local).AddTicks(6096) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.385402329193447, 60.397590069725823, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(775) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.382966325849466, 60.383387171629536, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(4253) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.391460843669009, 60.398584163014519, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(4278) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.387911788830614, 60.389456805867326, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(4288) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.392779461177568, 60.380607730183655, new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(7669) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1003610034, "SeedCode0006", 1003, 10034, 60.389786542279538, 60.394125468300238, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(7756), 10036 },
                    { 1003310031, "SeedCode0003", 1003, 10031, 60.389058447995232, 60.380603796461813, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(7731), 10033 },
                    { 1003210031, "SeedCode0002", 1003, 10031, 60.390280906264671, 60.397758926460419, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(7693), 10032 },
                    { 1002610025, "SeedCode0006", 1002, 10025, 60.399525794418629, 60.389318175545206, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(4306), 10026 },
                    { 1002510022, "SeedCode0005", 1002, 10022, 60.395663043571574, 60.383664536426402, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(4297), 10025 },
                    { 1001610014, "SeedCode0006", 1001, 10014, 60.395612670917927, 60.390885259799795, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(835), 10016 },
                    { 1001510011, "SeedCode0005", 1001, 10011, 60.397769636199492, 60.390475563347742, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(825), 10015 },
                    { 1001410011, "SeedCode0004", 1001, 10011, 60.390747669023163, 60.387793465208119, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(814), 10014 },
                    { 1001210013, "SeedCode0002", 1001, 10013, 60.393031373346538, 60.398662384851292, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(805), 10012 },
                    { 1000610001, "SeedCode0006", 1000, 10001, 60.387395040390501, 60.395885708197362, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 643, DateTimeKind.Local).AddTicks(6130), 10006 },
                    { 1000510002, "SeedCode0005", 1000, 10002, 60.399320696459817, 60.388522976060777, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 643, DateTimeKind.Local).AddTicks(6115), 10005 },
                    { 1003410035, "SeedCode0004", 1003, 10035, 60.385247739333025, 60.396234759774046, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 644, DateTimeKind.Local).AddTicks(7746), 10034 },
                    { 1000310002, "SeedCode0003", 1000, 10002, 60.38417622092107, 60.394200217301652, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 19, 54, 8, 643, DateTimeKind.Local).AddTicks(5889), 10003 }
                });
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class addsquadseeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610004);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410012);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510024);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610032);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "d4ff2b1c-8a2a-4c22-80f4-9e8ba84c7c77", "7e83616c-3082-487f-9072-78a9a5eb47d6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "59bb9670-4db7-4522-af9d-2d9fac762fb1", "20f110a8-6c70-4637-b490-30eefdc49bb7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "65013bc1-3ad7-40a6-8897-8cdcd462d389", "7e427eb7-87b8-404d-b301-223e9c7f5cdb" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "3635870a-fba3-4fe7-955a-d1ff96623174", "11b3b8f3-d44a-4652-a53d-a823407ca20f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "015950f9-112e-4189-baa6-045f04ce384c", "3ad87954-5876-454f-abf9-381dafd8418e" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "34dafeac-3678-4cf9-971b-bc7bb2987f0b", "ac08d01e-3324-40d0-a263-b2b1e7cbeedd" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.393901049098432, 5.298640459541371, new DateTime(2020, 4, 6, 8, 0, 26, 220, DateTimeKind.Local).AddTicks(4478) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38989377347044, 5.351595663234507, new DateTime(2020, 4, 6, 8, 0, 26, 222, DateTimeKind.Local).AddTicks(8478) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.380715508977673, 5.3126905740752841, new DateTime(2020, 4, 6, 8, 0, 26, 222, DateTimeKind.Local).AddTicks(8615) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510003,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.39685733602046, 5.3299549969027611, new DateTime(2020, 4, 6, 8, 0, 26, 222, DateTimeKind.Local).AddTicks(8632) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.386645575038173, 5.3192649316938461, new DateTime(2020, 4, 6, 8, 0, 26, 223, DateTimeKind.Local).AddTicks(5623) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.392784504943045, 5.3280953611900355, new DateTime(2020, 4, 6, 8, 0, 26, 223, DateTimeKind.Local).AddTicks(5650) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.386919919617931, 5.32123457087273, new DateTime(2020, 4, 6, 8, 0, 26, 223, DateTimeKind.Local).AddTicks(5680) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.399371837287894, 5.3400096395001908, new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(1154) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.398497013726939, 5.2960351385849034, new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(6668) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310032,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.385659150128362, 5.3397159747659106, new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(6702) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1001410011, "SeedCode0004", 1001, 10011, 60.385521202872269, 5.3441867427918606, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 223, DateTimeKind.Local).AddTicks(5660), 10014 },
                    { 1001510011, "SeedCode0005", 1001, 10011, 60.398861622532763, 5.3457964795039024, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 223, DateTimeKind.Local).AddTicks(5671), 10015 },
                    { 1002310022, "SeedCode0003", 1002, 10022, 60.388747124391259, 5.3056420055145397, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(1181), 10023 },
                    { 1002410022, "SeedCode0004", 1002, 10022, 60.394598465860291, 5.3187140127086927, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(1191), 10024 },
                    { 1003610033, "SeedCode0006", 1003, 10033, 60.38383079900381, 5.3213187817742744, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(6723), 10036 },
                    { 1002510023, "SeedCode0005", 1002, 10023, 60.399793799998776, 5.3440493817135017, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(1199), 10025 },
                    { 1002610021, "SeedCode0006", 1002, 10021, 60.382725822240694, 5.2999520857683606, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(1207), 10026 },
                    { 1003210031, "SeedCode0002", 1003, 10031, 60.382643951400446, 5.3537639071544936, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(6693), 10032 },
                    { 1003410035, "SeedCode0004", 1003, 10035, 60.390562604384918, 5.3500731202430671, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(6714), 10034 },
                    { 1000610002, "SeedCode0006", 1000, 10002, 60.380471569976883, 5.3544393381449087, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 222, DateTimeKind.Local).AddTicks(8649), 10006 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510023);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610033);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "8afade78-6878-4e36-b1b5-b8afffbcda84", "dd83f601-4e65-4dba-85dc-f46be06b8755" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a954bb93-b591-467d-8cbd-9704f7ff5e75", "53542d9f-737f-4530-9ee3-d3a7403472fe" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "601879b8-94da-4138-83ab-0380cae84d36", "94650ec8-7bde-45ab-9632-1ac2f7192028" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "9e3c3828-61e1-4353-9bfc-1f8b8f4ee76b", "dcadc361-8fc9-408c-8f21-afd7e1015477" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "32337f43-c8e3-428c-8632-a671b9b670eb", "a4b87659-8277-41b6-95a8-3b2c38f99b44" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "39b671e9-6688-4b55-b7e2-f98d5009ee6b", "e172b4b0-173e-4a6d-b55b-be984e0a67d0" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.386817482818614, 5.3231979326388972, new DateTime(2020, 4, 5, 23, 39, 59, 902, DateTimeKind.Local).AddTicks(6885) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.391440620405966, 5.3254789943861338, new DateTime(2020, 4, 5, 23, 39, 59, 905, DateTimeKind.Local).AddTicks(8861) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.391372699021076, 5.3042746112285579, new DateTime(2020, 4, 5, 23, 39, 59, 905, DateTimeKind.Local).AddTicks(9165) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510003,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.396275886885697, 5.3074523406638177, new DateTime(2020, 4, 5, 23, 39, 59, 905, DateTimeKind.Local).AddTicks(9185) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.399376952978017, 5.3212616466694866, new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(4392) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.380965773276415, 5.3256968759626151, new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(4420) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.385719555842485, 5.3504532234613551, new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(4451) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.398233160958945, 5.3120602471234042, new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(9079) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.380531860662465, 5.3021714176672194, new DateTime(2020, 4, 5, 23, 39, 59, 907, DateTimeKind.Local).AddTicks(2548) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310032,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.393533855957365, 5.3298688647109955, new DateTime(2020, 4, 5, 23, 39, 59, 907, DateTimeKind.Local).AddTicks(2584) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1001410012, "SeedCode0004", 1001, 10012, 60.395427448447073, 5.3194616881597936, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(4431), 10014 },
                    { 1001510013, "SeedCode0005", 1001, 10013, 60.399476848318841, 5.3384428784804721, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(4442), 10015 },
                    { 1002310021, "SeedCode0003", 1002, 10021, 60.395316877348755, 5.3044241557440071, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(9142), 10023 },
                    { 1002410021, "SeedCode0004", 1002, 10021, 60.393922093752515, 5.3327639149534507, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(9153), 10024 },
                    { 1003610032, "SeedCode0006", 1003, 10032, 60.398560218777924, 5.3143496442449312, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 907, DateTimeKind.Local).AddTicks(2607), 10036 },
                    { 1002510024, "SeedCode0005", 1002, 10024, 60.386043015594005, 5.3041543157948157, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(9162), 10025 },
                    { 1002610022, "SeedCode0006", 1002, 10022, 60.391016652218738, 5.3164598352078842, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 906, DateTimeKind.Local).AddTicks(9171), 10026 },
                    { 1003210035, "SeedCode0002", 1003, 10035, 60.38729060776415, 5.3435949890035719, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 907, DateTimeKind.Local).AddTicks(2575), 10032 },
                    { 1003410031, "SeedCode0004", 1003, 10031, 60.385076998492643, 5.3189907954297206, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 907, DateTimeKind.Local).AddTicks(2597), 10034 },
                    { 1000610004, "SeedCode0006", 1000, 10004, 60.383253438470554, 5.351175750358987, "Zombie bit human. The end.", new DateTime(2020, 4, 5, 23, 39, 59, 905, DateTimeKind.Local).AddTicks(9203), 10006 }
                });
        }
    }
}

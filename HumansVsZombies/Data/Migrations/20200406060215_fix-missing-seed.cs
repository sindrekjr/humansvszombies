﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class fixmissingseed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410022);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510023);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310032);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610033);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "49406eba-49bd-4efb-9233-a9c23213d294", "bae03e6d-812e-4af0-914b-9194741e4b1d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "b6bf66c2-7918-4a96-9ae9-274c60744293", "8827d4c2-d5da-4663-9c9d-0f9a31d7c994" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "75588880-bc20-4778-a246-93d819888af3", "c2b43578-fb65-4cdd-9414-f799a8ff0ea7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "3b938709-9565-4094-a481-85fe614b4269", "5414a9ae-e501-43fe-a69a-3533a3575a3c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "22345298-e549-4029-9d83-8a665c6b4b56", "aa4c8581-c484-4fba-a6a3-ff0985290042" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "3de8eced-a125-4ca7-8e93-1af35326b244", "041e00d9-005a-46f6-9966-e23bea097d8e" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.382752938384883, 5.3377898844351472, new DateTime(2020, 4, 6, 8, 2, 15, 183, DateTimeKind.Local).AddTicks(7445) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.386402539265873, 5.299751547526327, new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(1178) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.384653270756871, 5.2985053658341164, new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(7963) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.391075170683322, 5.3298946389456985, new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(8022) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.383654693835382, 5.332311491296724, new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(3446) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.391591451486065, 5.3181598889519766, new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(8860) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1003610034, "SeedCode0006", 1003, 10034, 60.388723860821997, 5.3479489346983966, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(8915), 10036 },
                    { 1000310002, "SeedCode0003", 1000, 10002, 60.382988211229559, 5.3495325757654708, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(1052), 10003 },
                    { 1000510001, "SeedCode0005", 1000, 10001, 60.384325964599803, 5.2968088880837971, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(1193), 10005 },
                    { 1000610004, "SeedCode0006", 1000, 10004, 60.39199160550254, 5.342119008136863, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(1210), 10006 },
                    { 1001210013, "SeedCode0002", 1001, 10013, 60.386212355939136, 5.3173755883470877, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(7992), 10012 },
                    { 1001410012, "SeedCode0004", 1001, 10012, 60.396096759528504, 5.3334873011417585, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(8002), 10014 },
                    { 1003310031, "SeedCode0003", 1003, 10031, 60.38637434569673, 5.330052267560613, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(8895), 10033 },
                    { 1001510013, "SeedCode0005", 1001, 10013, 60.394819464852752, 5.3310820513468835, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(8013), 10015 },
                    { 1002310021, "SeedCode0003", 1002, 10021, 60.397524906491029, 5.3507890595593244, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(3471), 10023 },
                    { 1002410021, "SeedCode0004", 1002, 10021, 60.394434396039514, 5.3384256481158641, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(3480), 10024 },
                    { 1002510024, "SeedCode0005", 1002, 10024, 60.387602138810202, 5.3096439577387393, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(3489), 10025 },
                    { 1002610025, "SeedCode0006", 1002, 10025, 60.384373172750927, 5.3028639222212153, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(3498), 10026 },
                    { 1003210035, "SeedCode0002", 1003, 10035, 60.39439868235656, 5.3550609475078792, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(8886), 10032 },
                    { 1003410032, "SeedCode0004", 1003, 10032, 60.397515944640688, 5.3205226096453417, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(8906), 10034 }
                });

            migrationBuilder.InsertData(
                table: "Squads",
                columns: new[] { "Id", "GameId", "IsHuman", "Name" },
                values: new object[,]
                {
                    { 10001, 1000, true, "Travelers" },
                    { 10051, 1005, true, "Military" },
                    { 10042, 1004, true, "Ancient Evil" },
                    { 10041, 1004, true, "Ash" },
                    { 10033, 1003, true, "AI" },
                    { 10032, 1003, true, "Scientists" },
                    { 10031, 1003, true, "Commandos" },
                    { 10023, 1002, true, "Army" },
                    { 10021, 1002, true, "Loners" },
                    { 10013, 1001, true, "Hunters" },
                    { 10012, 1001, true, "Marauders" },
                    { 10011, 1001, true, "Prisoners" },
                    { 10052, 1005, true, "Ladies" },
                    { 10022, 1002, true, "Shoppers" }
                });

            migrationBuilder.InsertData(
                table: "SquadMembers",
                columns: new[] { "Id", "GameId", "PlayerId", "Rank", "SquadId" },
                values: new object[,]
                {
                    { 1000110001, 1000, 10001, 0, 10001 },
                    { 1005110053, 1005, 10053, 0, 10051 },
                    { 1005110052, 1005, 10052, 0, 10051 },
                    { 1004210044, 1004, 10044, 0, 10042 },
                    { 1004210043, 1004, 10043, 0, 10042 },
                    { 1004110042, 1004, 10042, 0, 10041 },
                    { 1004110041, 1004, 10041, 0, 10041 },
                    { 1003310034, 1003, 10034, 0, 10033 },
                    { 1003310035, 1003, 10035, 0, 10033 },
                    { 1003210032, 1003, 10032, 0, 10032 },
                    { 1003210033, 1003, 10033, 0, 10032 },
                    { 1003110036, 1003, 10036, 0, 10031 },
                    { 1003110031, 1003, 10031, 0, 10031 },
                    { 1002310026, 1002, 10026, 0, 10023 },
                    { 1002310025, 1002, 10025, 0, 10023 },
                    { 1002210024, 1002, 10024, 0, 10022 },
                    { 1002210023, 1002, 10023, 0, 10022 },
                    { 1002110022, 1002, 10022, 0, 10021 },
                    { 1002110021, 1002, 10021, 0, 10021 },
                    { 1001310016, 1001, 10016, 0, 10013 },
                    { 1001310015, 1001, 10015, 0, 10013 },
                    { 1001210014, 1001, 10014, 0, 10012 },
                    { 1001210013, 1001, 10013, 0, 10012 },
                    { 1001110012, 1001, 10012, 0, 10011 },
                    { 1001110011, 1001, 10011, 0, 10011 },
                    { 1000110006, 1000, 10006, 0, 10001 },
                    { 1000110005, 1000, 10005, 0, 10001 },
                    { 1000110004, 1000, 10004, 0, 10001 },
                    { 1000110003, 1000, 10003, 0, 10001 },
                    { 1000110002, 1000, 10002, 0, 10001 },
                    { 1005210051, 1005, 10051, 0, 10052 },
                    { 1005210054, 1005, 10054, 0, 10052 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610004);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410012);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510024);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610025);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310031);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410032);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610034);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1000110001);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1000110002);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1000110003);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1000110004);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1000110005);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1000110006);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1001110011);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1001110012);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1001210013);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1001210014);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1001310015);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1001310016);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1002110021);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1002110022);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1002210023);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1002210024);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1002310025);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1002310026);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1003110031);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1003110036);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1003210032);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1003210033);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1003310034);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1003310035);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1004110041);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1004110042);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1004210043);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1004210044);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1005110052);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1005110053);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1005210051);

            migrationBuilder.DeleteData(
                table: "SquadMembers",
                keyColumn: "Id",
                keyValue: 1005210054);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10001);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10011);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10012);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10013);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10021);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10022);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10023);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10031);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10032);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10033);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10041);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10042);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10051);

            migrationBuilder.DeleteData(
                table: "Squads",
                keyColumn: "Id",
                keyValue: 10052);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "d4ff2b1c-8a2a-4c22-80f4-9e8ba84c7c77", "7e83616c-3082-487f-9072-78a9a5eb47d6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "59bb9670-4db7-4522-af9d-2d9fac762fb1", "20f110a8-6c70-4637-b490-30eefdc49bb7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "65013bc1-3ad7-40a6-8897-8cdcd462d389", "7e427eb7-87b8-404d-b301-223e9c7f5cdb" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "3635870a-fba3-4fe7-955a-d1ff96623174", "11b3b8f3-d44a-4652-a53d-a823407ca20f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "015950f9-112e-4189-baa6-045f04ce384c", "3ad87954-5876-454f-abf9-381dafd8418e" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "34dafeac-3678-4cf9-971b-bc7bb2987f0b", "ac08d01e-3324-40d0-a263-b2b1e7cbeedd" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.393901049098432, 5.298640459541371, new DateTime(2020, 4, 6, 8, 0, 26, 220, DateTimeKind.Local).AddTicks(4478) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.380715508977673, 5.3126905740752841, new DateTime(2020, 4, 6, 8, 0, 26, 222, DateTimeKind.Local).AddTicks(8615) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.386645575038173, 5.3192649316938461, new DateTime(2020, 4, 6, 8, 0, 26, 223, DateTimeKind.Local).AddTicks(5623) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.386919919617931, 5.32123457087273, new DateTime(2020, 4, 6, 8, 0, 26, 223, DateTimeKind.Local).AddTicks(5680) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.399371837287894, 5.3400096395001908, new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(1154) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.398497013726939, 5.2960351385849034, new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(6668) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1003610033, "SeedCode0006", 1003, 10033, 60.38383079900381, 5.3213187817742744, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(6723), 10036 },
                    { 1003410035, "SeedCode0004", 1003, 10035, 60.390562604384918, 5.3500731202430671, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(6714), 10034 },
                    { 1003210031, "SeedCode0002", 1003, 10031, 60.382643951400446, 5.3537639071544936, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(6693), 10032 },
                    { 1002610021, "SeedCode0006", 1002, 10021, 60.382725822240694, 5.2999520857683606, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(1207), 10026 },
                    { 1002510023, "SeedCode0005", 1002, 10023, 60.399793799998776, 5.3440493817135017, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(1199), 10025 },
                    { 1002410022, "SeedCode0004", 1002, 10022, 60.394598465860291, 5.3187140127086927, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(1191), 10024 },
                    { 1002310022, "SeedCode0003", 1002, 10022, 60.388747124391259, 5.3056420055145397, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(1181), 10023 },
                    { 1001510011, "SeedCode0005", 1001, 10011, 60.398861622532763, 5.3457964795039024, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 223, DateTimeKind.Local).AddTicks(5671), 10015 },
                    { 1001410011, "SeedCode0004", 1001, 10011, 60.385521202872269, 5.3441867427918606, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 223, DateTimeKind.Local).AddTicks(5660), 10014 },
                    { 1001210011, "SeedCode0002", 1001, 10011, 60.392784504943045, 5.3280953611900355, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 223, DateTimeKind.Local).AddTicks(5650), 10012 },
                    { 1000610002, "SeedCode0006", 1000, 10002, 60.380471569976883, 5.3544393381449087, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 222, DateTimeKind.Local).AddTicks(8649), 10006 },
                    { 1000510003, "SeedCode0005", 1000, 10003, 60.39685733602046, 5.3299549969027611, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 222, DateTimeKind.Local).AddTicks(8632), 10005 },
                    { 1003310032, "SeedCode0003", 1003, 10032, 60.385659150128362, 5.3397159747659106, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 224, DateTimeKind.Local).AddTicks(6702), 10033 },
                    { 1000310001, "SeedCode0003", 1000, 10001, 60.38989377347044, 5.351595663234507, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 0, 26, 222, DateTimeKind.Local).AddTicks(8478), 10003 }
                });
        }
    }
}

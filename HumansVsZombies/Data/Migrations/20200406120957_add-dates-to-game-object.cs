﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HumansVsZombies.Data.Migrations
{
    public partial class adddatestogameobject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610004);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610013);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510024);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610025);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410032);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndTime",
                table: "Games",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "StartTime",
                table: "Games",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "c4cdadca-492e-4f4c-85cb-4b1904d63092", "511404b8-1cc1-4948-af71-2d8a88df974c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "5c70a739-50e1-4ed1-804f-b331607e043c", "36520444-f9c1-418b-9bf0-8dd71d266859" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a19db815-b700-4957-b32f-bdb0eeebdade", "95111f7b-10ea-439c-b2eb-372b0aff74f0" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "9a93e028-4ac2-4845-9f08-56532ef43292", "7868d4c1-0f61-427a-a748-18bac060a09d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "e847e5dc-8f3b-41a7-b9a5-04c4b6298e3f", "2e65d580-5ca9-4ce7-ac7d-d66c664bdfbd" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "26b555d4-1fbf-467f-abb1-923f3e3d0140", "388caf0a-8820-457c-91ff-42c7b6ac530c" });

            migrationBuilder.UpdateData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1000,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2019, 4, 6, 16, 9, 57, 34, DateTimeKind.Local).AddTicks(5367), new DateTime(2019, 4, 6, 14, 9, 57, 32, DateTimeKind.Local).AddTicks(4650) });

            migrationBuilder.UpdateData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1001,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2019, 6, 6, 18, 9, 57, 37, DateTimeKind.Local).AddTicks(7665), new DateTime(2019, 6, 6, 14, 9, 57, 37, DateTimeKind.Local).AddTicks(7646) });

            migrationBuilder.UpdateData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1002,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2019, 6, 15, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(3686), new DateTime(2019, 6, 14, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(3645) });

            migrationBuilder.UpdateData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1003,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2019, 11, 7, 2, 9, 57, 38, DateTimeKind.Local).AddTicks(9620), new DateTime(2019, 11, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(9602) });

            migrationBuilder.UpdateData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1004,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 6, 6, 14, 9, 57, 39, DateTimeKind.Local).AddTicks(3990), new DateTime(2020, 5, 6, 14, 9, 57, 39, DateTimeKind.Local).AddTicks(3974) });

            migrationBuilder.UpdateData(
                table: "Games",
                keyColumn: "Id",
                keyValue: 1005,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2020, 7, 6, 20, 9, 57, 39, DateTimeKind.Local).AddTicks(8602), new DateTime(2020, 7, 6, 14, 9, 57, 39, DateTimeKind.Local).AddTicks(8585) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.388462673536161, 5.3113519372307039, new DateTime(2020, 4, 6, 14, 9, 57, 36, DateTimeKind.Local).AddTicks(8153) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.39548971491601, 5.3482733978347987, new DateTime(2020, 4, 6, 14, 9, 57, 37, DateTimeKind.Local).AddTicks(7827) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410012,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.387692637027584, 5.2983201062366057, new DateTime(2020, 4, 6, 14, 9, 57, 37, DateTimeKind.Local).AddTicks(7892) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.381688146889502, 5.3225414245670768, new DateTime(2020, 4, 6, 14, 9, 57, 37, DateTimeKind.Local).AddTicks(7904) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.399401151004291, 5.3524464908910359, new DateTime(2020, 4, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(3791) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.385350747368093, 5.3422220882963307, new DateTime(2020, 4, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(3805) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38107552480998, 5.324885962515796, new DateTime(2020, 4, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(3814) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38632453774602, 5.338282117333776, new DateTime(2020, 4, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(9753) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.390834031146603, 5.3024673643922293, new DateTime(2020, 4, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(9767) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310031,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.381795458636731, 5.3223141264008138, new DateTime(2020, 4, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(9777) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610034,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.384281310663887, 5.3020360085027667, new DateTime(2020, 4, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(9798) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1003410033, "SeedCode0004", 1003, 10033, 60.395079830565834, 5.3203792742069327, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(9788), 10034 },
                    { 1002610021, "SeedCode0006", 1002, 10021, 60.391518105260481, 5.3278919858821858, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(3830), 10026 },
                    { 1002510021, "SeedCode0005", 1002, 10021, 60.389925045134405, 5.3336312123940779, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 14, 9, 57, 38, DateTimeKind.Local).AddTicks(3822), 10025 },
                    { 1001610011, "SeedCode0006", 1001, 10011, 60.385959715137929, 5.3314210521508869, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 14, 9, 57, 37, DateTimeKind.Local).AddTicks(7913), 10016 },
                    { 1001210011, "SeedCode0002", 1001, 10011, 60.392815422099211, 5.3289338260421673, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 14, 9, 57, 37, DateTimeKind.Local).AddTicks(7878), 10012 },
                    { 1000610005, "SeedCode0006", 1000, 10005, 60.380530058112463, 5.336244979562645, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 14, 9, 57, 37, DateTimeKind.Local).AddTicks(942), 10006 },
                    { 1000510003, "SeedCode0005", 1000, 10003, 60.387850258898716, 5.3551024658616022, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 14, 9, 57, 37, DateTimeKind.Local).AddTicks(927), 10005 },
                    { 1000410002, "SeedCode0004", 1000, 10002, 60.392921067113711, 5.3021145827077856, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 14, 9, 57, 37, DateTimeKind.Local).AddTicks(914), 10004 },
                    { 1000310001, "SeedCode0003", 1000, 10001, 60.397233297489514, 5.3284583051205709, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 14, 9, 57, 37, DateTimeKind.Local).AddTicks(807), 10003 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000310001);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000410002);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000510003);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000610005);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001210011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001610011);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002510021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002610021);

            migrationBuilder.DeleteData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003410033);

            migrationBuilder.DropColumn(
                name: "EndTime",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "StartTime",
                table: "Games");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0001",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "49406eba-49bd-4efb-9233-a9c23213d294", "bae03e6d-812e-4af0-914b-9194741e4b1d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0002",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "b6bf66c2-7918-4a96-9ae9-274c60744293", "8827d4c2-d5da-4663-9c9d-0f9a31d7c994" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0003",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "75588880-bc20-4778-a246-93d819888af3", "c2b43578-fb65-4cdd-9414-f799a8ff0ea7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0004",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "3b938709-9565-4094-a481-85fe614b4269", "5414a9ae-e501-43fe-a69a-3533a3575a3c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0005",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "22345298-e549-4029-9d83-8a665c6b4b56", "aa4c8581-c484-4fba-a6a3-ff0985290042" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "0006",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "3de8eced-a125-4ca7-8e93-1af35326b244", "041e00d9-005a-46f6-9966-e23bea097d8e" });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1000110002,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.382752938384883, 5.3377898844351472, new DateTime(2020, 4, 6, 8, 2, 15, 183, DateTimeKind.Local).AddTicks(7445) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001110013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.384653270756871, 5.2985053658341164, new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(7963) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001410012,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.396096759528504, 5.3334873011417585, new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(8002) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1001510013,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.394819464852752, 5.3310820513468835, new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(8013) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002210021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.383654693835382, 5.332311491296724, new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(3446) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002310021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.397524906491029, 5.3507890595593244, new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(3471) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1002410021,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.394434396039514, 5.3384256481158641, new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(3480) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003110035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.391591451486065, 5.3181598889519766, new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(8860) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003210035,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.39439868235656, 5.3550609475078792, new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(8886) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003310031,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.38637434569673, 5.330052267560613, new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(8895) });

            migrationBuilder.UpdateData(
                table: "Kills",
                keyColumn: "Id",
                keyValue: 1003610034,
                columns: new[] { "Lat", "Lng", "TimeOfDeath" },
                values: new object[] { 60.388723860821997, 5.3479489346983966, new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(8915) });

            migrationBuilder.InsertData(
                table: "Kills",
                columns: new[] { "Id", "BiteCode", "GameId", "KillerId", "Lat", "Lng", "Story", "TimeOfDeath", "VictimId" },
                values: new object[,]
                {
                    { 1002610025, "SeedCode0006", 1002, 10025, 60.384373172750927, 5.3028639222212153, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(3498), 10026 },
                    { 1002510024, "SeedCode0005", 1002, 10024, 60.387602138810202, 5.3096439577387393, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(3489), 10025 },
                    { 1001610013, "SeedCode0006", 1001, 10013, 60.391075170683322, 5.3298946389456985, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(8022), 10016 },
                    { 1001210013, "SeedCode0002", 1001, 10013, 60.386212355939136, 5.3173755883470877, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(7992), 10012 },
                    { 1000610004, "SeedCode0006", 1000, 10004, 60.39199160550254, 5.342119008136863, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(1210), 10006 },
                    { 1000510001, "SeedCode0005", 1000, 10001, 60.384325964599803, 5.2968088880837971, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(1193), 10005 },
                    { 1000410003, "SeedCode0004", 1000, 10003, 60.386402539265873, 5.299751547526327, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(1178), 10004 },
                    { 1003410032, "SeedCode0004", 1003, 10032, 60.397515944640688, 5.3205226096453417, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 187, DateTimeKind.Local).AddTicks(8906), 10034 },
                    { 1000310002, "SeedCode0003", 1000, 10002, 60.382988211229559, 5.3495325757654708, "Zombie bit human. The end.", new DateTime(2020, 4, 6, 8, 2, 15, 186, DateTimeKind.Local).AddTicks(1052), 10003 }
                });
        }
    }
}

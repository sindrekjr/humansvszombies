﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HumansVsZombies.Models
{
    public class Game
    {
        public int Id { get; set; }

        /// Metadata
        public string Name { get; set; }
        public virtual GameState GameState { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual DateTime EndTime { get; set; }
        public double? NELat { get; set; }
        public double? NELng { get; set; }
        public double? SWLat { get; set; }
        public double? SWLng { get; set; }

        /// Collections
        public virtual ICollection<Kill> Kills { get; set; }
        public virtual ICollection<Player> Players { get; set; }
        public virtual ICollection<SquadMember> SquadMembers { get; set; }
        public virtual ICollection<Squad> Squads { get; set; }
        public virtual ICollection<SquadCheckIn> SquadCheckIns { get; set; }
        public virtual ICollection<Mission> Missions { get; set; }
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum GameState
    {
        Registration,
        InProgress,
        Complete
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HumansVsZombies.Models
{
    public class Kill
    {
        public int Id { get; set; }

        /// Killer
        [ForeignKey("Killer")]
        public int KillerId { get; set; }
        [JsonIgnore]
        public virtual Player Killer { get; set; }

        /// Victim
        [ForeignKey("Victim")]
        public int VictimId { get; set; }
        [JsonIgnore]
        public virtual Player Victim { get; set; }
        public string BiteCode { get; set; }

        /// Game
        public int GameId { get; set; }
        [JsonIgnore]
        public virtual Game Game { get; set; }


        /// Metadata
        public virtual DateTime TimeOfDeath { get; set; }
        public string Story { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HumansVsZombies.Models
{
    public class Mission
    {
        public int Id { get; set; }

        /// Game
        public int GameId { get; set; }
        [JsonIgnore]
        public virtual Game Game { get; set; }


        /// Metadata
        public string Name { get; set; }
        public bool IsHumanVisible { get; set; }
        public bool IsZombieVisible { get; set; }
        
        #nullable enable
        public string? Description { get; set; }
        public virtual DateTime? StartTime { get; set; }
        public virtual DateTime? EndTime { get; set; }
        #nullable disable
    }
}

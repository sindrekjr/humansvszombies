﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HumansVsZombies.Models
{
    public class Player
    {
        public int Id { get; set; }
        
        /// User
        public string UserId { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }

        /// Game
        public int GameId { get; set; }
        [JsonIgnore]
        public virtual Game Game { get; set; }

        /// Death :: a reference to the player's death if they have been killed
        public virtual Kill Death { get; set; }

        
        /// Metadata
        public string PlayerName { get; set; }
        public string BiteCode { get; set; }
        public bool IsHuman { get; set; }
        public bool IsPatientZero { get; set; }
        public bool IsGameAdmin { get; set; }

        public virtual ICollection<Kill> Kills { get; set; }
        public virtual ICollection<SquadMember> SquadMembers { get; set; }
    } 
}

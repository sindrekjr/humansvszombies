﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HumansVsZombies.Models
{
    public class Squad
    {
        public int Id { get; set; }
        
        /// Game
        public int GameId { get; set; }
        [JsonIgnore]
        public virtual Game Game { get; set; }


        /// Metadata
        public string Name { get; set; }
        public bool IsHuman { get; set; } = true;
        public virtual ICollection<SquadMember> SquadMembers { get; set; }
        public virtual ICollection<SquadCheckIn> SquadCheckIns { get; set; }

        public bool HasMember(Player player)
        {
            foreach(var member in SquadMembers) if (member.PlayerId == player.Id) return true;
            return false;
        }
    }
}

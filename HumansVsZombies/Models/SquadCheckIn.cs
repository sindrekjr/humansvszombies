﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HumansVsZombies.Models
{
    public class SquadCheckIn
    {
        public int Id { get; set; }

        /// SquadMember
        public int SquadMemberId { get; set; }
        [JsonIgnore]
        public virtual SquadMember SquadMember { get; set; }

        /// Squad
        public int SquadId { get; set; }
        [JsonIgnore]
        public virtual Squad Squad { get; set; }

        /// Game
        public int GameId { get; set; }
        [JsonIgnore]
        public virtual Game Game { get; set; }


        /// Metadata
        public virtual DateTime StartTime { get; set; }
        public virtual DateTime EndTime { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}

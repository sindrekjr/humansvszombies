﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HumansVsZombies.Models
{
    public class SquadMember
    {
        public int Id { get; set; }
        
        /// Player
        public int PlayerId { get; set; }
        [JsonIgnore]
        public virtual Player Player { get; set; }
        public virtual string PlayerName { get => Player.PlayerName; }
        
        /// Squad
        public int SquadId { get; set; }
        [JsonIgnore]
        public virtual Squad Squad { get; set; }

        /// Game
        public int GameId { get; set; }
        [JsonIgnore]
        public virtual Game Game { get; set; }

        
        /// Metadata
        public Rank Rank { get; set; }
        public virtual ICollection<SquadCheckIn> SquadCheckIns { get; set; }
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum Rank { Member, Leader }
}

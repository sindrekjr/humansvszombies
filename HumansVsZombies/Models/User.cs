﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HumansVsZombies.Models
{
    public class User: IdentityUser
    {
        /// Metadata
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<Player> Players { get; set; }
    }
}

# Humans Vs. Zombies

ASP.NET Application with React frontend. 


## Requirements
* .NET Core 3.1
* Nodejs v??


## Features
* Authentication
* Game state management
* Global and faction-specific chat
* Maps and statistics
// More??


## Configuration & Installation
The project expects the following environment variables and configurations to be set. 
* WEBSITE_FULL_HOSTNAME (env)
* ConnectionStrings.DefaultConnection (env)
* Google Authentication Client Id
* Google Authentication Client Secret
// More??

### To run
// Missing steps


## Related Documentation
* Nodejs
* SignalR
* Entity Framework
* Google Maps React
// More??

## Maintainers

* [Lars-Martin Skog Solbak](https://gitlab.com/larsmartinskogsolbak)
* [Sindre Sperrud Kjær](https://gitlab.com/sindrekjr)
* [Sondré Elde](https://gitlab.com/sondreelde)
* [Ådne Skeie](https://gitlab.com/adnske7)
